package ac.soton.codegen.core.basis;

import org.eventb.core.EventBAttributes;
import org.eventb.core.basis.EventBRoot;
import org.rodinp.core.IAttributeType;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinDBException;
import org.rodinp.core.basis.RodinElement;
import ac.soton.codegen.core.ISCScheduleRoot;
import ac.soton.codegen.core.IScheduleRoot;

public class ScheduleRoot extends EventBRoot implements IScheduleRoot{

	public ScheduleRoot(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}
	
	@Override
	public boolean hasAttribute(IAttributeType type) throws RodinDBException {
		return false;
	}
	
	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public IRodinFile getSCScheduleFile(String bareName) {
		String fileName = DatabaseUtilities.getSCScheduleFullName(bareName);
		IRodinFile file = getRodinProject().getRodinFile(fileName);
		return file;
	}

	@Override
	public ISCScheduleRoot getSCScheduleRoot() {
		return getSCScheduleRoot(getElementName());
	}

	@Override
	public ISCScheduleRoot getSCScheduleRoot(String bareName) {
		ISCScheduleRoot root = (ISCScheduleRoot) getSCScheduleFile(bareName)
				.getRoot();
		try {
			root.setAttributeValue(EventBAttributes.CONFIGURATION_ATTRIBUTE, "csConfig" ,null);
		} catch (RodinDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return root;
	}

	public RodinElement[] getChildren() throws RodinDBException {
		final RodinElement[] children = new RodinElement[0];
		// Must make a copy as we don't want to expose the internal array
//		if (children.length == 0) {
//			return NO_ELEMENTS;
//		}
		return children;
	}

}
