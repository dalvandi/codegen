package ac.soton.codegen.core.basis;

import org.eventb.core.basis.SCPredicateElement;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;

import ac.soton.codegen.core.ISCSchedule;

public class SCSchedule extends SCPredicateElement implements ISCSchedule{

	public SCSchedule(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}

}
