package ac.soton.codegen.core.basis;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.basis.EventBElement;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.RodinDBException;
import ac.soton.codegen.core.ScheduleAttributes;
import ac.soton.codegen.core.ISchedule;

public class Schedule extends EventBElement implements ISchedule{

	public Schedule(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}

	@Override
	public boolean hasScheduleRefine() throws RodinDBException {
		return hasAttribute(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE);
	}

	@Override
	public void setScheduleName(String name, IProgressMonitor monitor) throws RodinDBException {
		setAttributeValue(ScheduleAttributes.SCHEDULE_NAME_ATTRIBUTE, name, monitor);
		
	}

	@Override
	public void setScheduleRefine(String refine, IProgressMonitor monitor) throws RodinDBException {
		setAttributeValue(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE, refine, monitor);
	}

	@Override
	public void setMachineName(String name, IProgressMonitor monitor) throws RodinDBException {
		setAttributeValue(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE, name, monitor);
	}

	@Override
	public String getScheduleName() throws RodinDBException {
		return getAttributeValue(ScheduleAttributes.SCHEDULE_NAME_ATTRIBUTE);	
	}

	@Override
	public String getScheduleRefine() throws RodinDBException {
		return getAttributeValue(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE);	
	}

	@Override
	public String getMachineName() throws RodinDBException {
		return getAttributeValue(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE);	
	}

}
