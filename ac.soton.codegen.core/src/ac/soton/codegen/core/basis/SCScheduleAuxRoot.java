/*******************************************************************************
 * Copyright (c) 2010 University of Southampton.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package ac.soton.codegen.core.basis;

import org.eventb.core.ast.FormulaFactory;
import org.eventb.core.ast.ITypeEnvironment;
import org.eventb.core.ast.ITypeEnvironmentBuilder;
import org.eventb.core.basis.EventBRoot;

import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.IScheduleAuxRoot;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinDBException;


public class SCScheduleAuxRoot extends EventBRoot implements ISCScheduleAuxRoot {

	public SCScheduleAuxRoot(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public ITypeEnvironment getTypeEnvironment(FormulaFactory factory) throws RodinDBException {
		ITypeEnvironmentBuilder typeEnvironment = factory.makeTypeEnvironment();

		return typeEnvironment;
	}

	@Override
	public IRodinFile getScheduleAuxFile(String bareName) {
		String fileName = DatabaseUtilities.getScheduleAuxFullName(bareName);
		IRodinFile file = getRodinProject().getRodinFile(fileName);
		return file;
	}

	@Override
	public IScheduleAuxRoot getScheduleAuxRoot() {
		return getScheduleAuxRoot(getElementName());
	}

	@Override
	public IScheduleAuxRoot getScheduleAuxRoot(String bareName) {
		IScheduleAuxRoot root = (IScheduleAuxRoot) getScheduleAuxFile(bareName).getRoot();
		return root;
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}

}
