/*******************************************************************************
 * Copyright (c) 2010, 2013 University of Southampton.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     University of Southampton - initial API and implementation
 *******************************************************************************/
package ac.soton.codegen.core.basis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eventb.core.IPSStatus;
import org.eventb.core.seqprover.IConfidence;

import ac.soton.codegen.core.ISCScheduleRoot;
import ac.soton.codegen.core.IScheduleRoot;
import ac.soton.codegen.core.plugin.CodegenPlugin;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

/**
 * Accessibility class for some fields and methods for other plug-ins. Includes
 * mostly Rodin database related facilities.
 * 
 * @since 1.0
 * 
 * @author maamria
 * 
 */
public class DatabaseUtilities {
	
	public static final String SCHEDULE_FILE_EXTENSION = "sax";
	public static final String SC_SCHEDULE_FILE_EXTENSION = "scx";
	public static final String SCHEDULE_CONFIGURATION = CodegenPlugin.PLUGIN_ID + ".sched";

	/**
	 * Returns the SC theories that are the children of the given project.
	 * 
	 * @param project
	 *            the rodin project
	 * @param filter
	 *            theories filter to apply
	 * @return SC theories
	 * @throws CoreException
	 */
	public static ISCScheduleRoot[] getSCScheduleRoots(IRodinProject project, IScheduleFilter<ISCScheduleRoot> filter)
			throws CoreException {
		ISCScheduleRoot[] roots = project.getRootElementsOfType(ISCScheduleRoot.ELEMENT_TYPE);
		List<ISCScheduleRoot> okRoots = new ArrayList<ISCScheduleRoot>();
		for (ISCScheduleRoot root : roots) { 
			if (filter.filter(root)) {
				okRoots.add(root);
			}
		}
		return okRoots.toArray(new ISCScheduleRoot[okRoots.size()]);
	}

	/**
	 * Returns the SC theory parent of the given element if any.
	 * 
	 * @param element
	 *            the rodin element
	 * @return the parent theory
	 */
	public static ISCScheduleRoot getSCScheduleParent(IRodinElement element) {
		return element.getAncestor(ISCScheduleRoot.ELEMENT_TYPE);
	}

	/**
	 * Returns a set of string representations of the list of the given
	 * elements.
	 * 
	 * @param <E>
	 *            the type of elements
	 * @param elements
	 *            the actual elements
	 * @return the set
	 */
	public static <E extends IInternalElement> Set<String> getElementNames(Collection<E> elements) {
		Set<String> set = new LinkedHashSet<String>();
		for (E e : elements) {
			set.add(e.getElementName());
		}
		return set;
	}

	/**
	 * Returns whether the theory has errors.
	 * 
	 * @param root
	 *            the SC theory root
	 * @return whether the theory is not accurate
	 */
	public static boolean doesScheduleHaveErrors(ISCScheduleRoot root) {
		try {
			if (!root.exists() || (root.exists() && !root.isAccurate())) {
				return true;
			}
		} catch (RodinDBException e) {
			//CoreUtilities.log(e, "failed to retrieve details from theory.");
			return true;
		}
		return false;
	}



	public static ISCScheduleRoot getSCSchedule(String name, IRodinProject project) {
		IRodinFile file = project.getRodinFile(getSCScheduleFullName(name));
		return (ISCScheduleRoot) file.getRoot();
	}

	/**
	 * Returns a handle to the theory with the given name.
	 * 
	 * @param name
	 * @param project
	 * @return a handle to the theory
	 */
	public static IScheduleRoot getSchedule(String name, IRodinProject project) {
		IRodinFile file = project.getRodinFile(getScheduleFullName(name));
		return (IScheduleRoot) file.getRoot();
	}

	/**
	 * Returns the full name of a theory file.
	 * 
	 * @param bareName
	 *            the name
	 * @return the full name
	 */
	public static String getScheduleFullName(String bareName) {
		return bareName + "." + SCHEDULE_FILE_EXTENSION;
	}

	/**
	 * Returns the full name of a SC theory file.
	 * 
	 * @param bareName
	 *            the name
	 * @return the full name
	 */
	public static String getSCScheduleFullName(String bareName) {
		return bareName + "." + SC_SCHEDULE_FILE_EXTENSION;
	}

	/**
	 * Returns the project with the given name if it exists.
	 * 
	 * @param name
	 *            the project name
	 * @return the project or <code>null</code> if the project does not exist
	 */
	public static IRodinProject getRodinProject(String name) {
		IRodinProject project = RodinCore.getRodinDB().getRodinProject(name);
		if (project.exists())
			return project;
		else
			return null;
	}

	/**
	 * Returns whether the given rodin file originated from a theory file.
	 * 
	 * @param file
	 *            the rodin file
	 * @return whether the file originated from a theory
	 * @throws CoreException
	 */
	public static boolean originatedFromSchedule(IRodinFile file) {
		IRodinProject project = file.getRodinProject();
		IScheduleRoot theoryRoot = getSchedule(file.getBareName(), project);
		if (theoryRoot.exists()) {
			return true;
		}
		return false;
	}


	public static boolean originatedFromSchedule(IRodinFile file, IRodinProject project) {
		return file.getRodinProject().equals(project) && originatedFromSchedule(file);
	}


	public static void rebuild(final IRodinProject project, IProgressMonitor monitor) throws CoreException {
		if (project == null || !project.exists()) {
			throw new CoreException(new Status(IStatus.ERROR, CodegenPlugin.PLUGIN_ID,
					"attempting to rebuild a non-existent project"));
		}
		RodinCore.run(new IWorkspaceRunnable() {
			@Override
			public void run(IProgressMonitor monitor) throws CoreException {
				IProject nativeProj = ((IProject) project.getAdapter(IProject.class));
				if (nativeProj != null) {
					nativeProj.build(IncrementalProjectBuilder.CLEAN_BUILD, monitor);
				}
			}
		}, monitor);

	}


	/**
	 * Returns whether the given proof status is of discharged status.
	 * 
	 * @param status
	 *            proof status
	 * @return whether status PO has been discharged
	 * @throws RodinDBException
	 */
	public static boolean isDischarged(IPSStatus status) throws RodinDBException {
		return (status.getConfidence() > IConfidence.REVIEWED_MAX) && (!status.isBroken());
	}

	/**
	 * Returns whether the given proof status is of reviewed status.
	 * 
	 * @param status
	 *            proof status
	 * @return whether status PO has been reviewed
	 * @throws RodinDBException
	 */
	public static boolean isReviewed(IPSStatus status) throws RodinDBException {
		return (status.getConfidence() > IConfidence.PENDING) && (status.getConfidence() <= IConfidence.REVIEWED_MAX);
	}

	/**
	 * Returns non temporary statically checked theory path roots of the given
	 * project.
	 * 
	 * @param project
	 *            a Rodin project
	 * @return an array of theory path roots
	 * @throws RodinDBException
	 *             if there is a problem accessing the Rodin database
	 */
	
	public static IScheduleFilter<ISCScheduleRoot> getNonTempSCTheoriesFilter() {

		return new IScheduleFilter<ISCScheduleRoot>() {
			@Override
			public boolean filter(ISCScheduleRoot theory) {
				return theory.exists()
						&& !theory.getRodinFile().getElementName().endsWith(SC_SCHEDULE_FILE_EXTENSION + "_tmp");
			}
		};
	}

	
	public static IScheduleFilter<ISCScheduleRoot> getExistingSCTheoriesFilter() {

		return new IScheduleFilter<ISCScheduleRoot>() {
			@Override
			public boolean filter(ISCScheduleRoot theory) {
				return theory.exists();
			}
		};
	}

	public static String getScheduleAuxFullName(String bareName) {
		return bareName + "." + SCHEDULE_FILE_EXTENSION;
	}



}
