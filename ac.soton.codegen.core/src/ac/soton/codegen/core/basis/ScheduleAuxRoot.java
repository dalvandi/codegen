package ac.soton.codegen.core.basis;

import org.eventb.core.basis.EventBRoot;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.IScheduleAuxRoot;

public class ScheduleAuxRoot extends EventBRoot implements IScheduleAuxRoot {

	public ScheduleAuxRoot(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}

	@Override
	public ISCScheduleAuxRoot getSCScheduleAuxRoot() {
		return getSCScheduleAuxRoot(getElementName());
	}

	public ISCScheduleAuxRoot getSCScheduleAuxRoot(String bareName) {
		ISCScheduleAuxRoot root = (ISCScheduleAuxRoot) getSCScheduleAuxFile(bareName)
				.getRoot();
		return root;
	}

	public IRodinFile getSCScheduleAuxFile(String bareName) {
		String fileName = bareName + ".scx";
		IRodinFile file = getRodinProject().getRodinFile(fileName);
		return file;
	}


}
