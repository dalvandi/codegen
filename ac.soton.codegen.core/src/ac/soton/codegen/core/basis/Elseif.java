package ac.soton.codegen.core.basis;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.basis.EventBElement;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IElseif;
import ac.soton.codegen.core.ScheduleAttributes;


public class Elseif extends EventBElement implements IElseif{

	public Elseif(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}
	
	@Override
	public void setCond(String cond, IProgressMonitor monitor) throws RodinDBException {
		setAttributeValue(ScheduleAttributes.COND_ATTRIBUTE, cond, monitor);
		
	}

	@Override
	public String getCond() throws RodinDBException {
		return getAttributeValue(ScheduleAttributes.COND_ATTRIBUTE);	
	}

}
