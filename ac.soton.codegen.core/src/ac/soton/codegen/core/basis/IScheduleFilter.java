package ac.soton.codegen.core.basis;

import org.rodinp.core.IInternalElement;

public interface IScheduleFilter<T extends IInternalElement> {

	public boolean filter(T schedule);

}
