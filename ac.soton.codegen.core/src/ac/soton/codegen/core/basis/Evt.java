package ac.soton.codegen.core.basis;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.basis.EventBElement;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IEvt;
import ac.soton.codegen.core.ScheduleAttributes;

public class Evt extends EventBElement implements IEvt{

	public Evt(String name, IRodinElement parent) {
		super(name, parent);
	}

	@Override
	public IInternalElementType<? extends IInternalElement> getElementType() {
		return ELEMENT_TYPE;
	}

	@Override
	public void setEventName(String name, IProgressMonitor monitor) throws RodinDBException {
		setAttributeValue(ScheduleAttributes.EVENT_NAME_ATTRIBUTE, name, monitor);
		
	}

	@Override
	public String getEventName() throws RodinDBException {
		return getAttributeValue(ScheduleAttributes.EVENT_NAME_ATTRIBUTE);	
	}
	
	
	
}