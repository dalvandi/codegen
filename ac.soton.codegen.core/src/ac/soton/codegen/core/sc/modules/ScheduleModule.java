package ac.soton.codegen.core.sc.modules;


import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.sc.SCCore;
import org.eventb.core.sc.SCProcessorModule;
import org.eventb.core.sc.state.ISCStateRepository;
import org.eventb.core.tool.IModuleType;
import ac.soton.codegen.core.ISCSchedule;
import ac.soton.codegen.core.ISchedule;
import ac.soton.codegen.core.IScheduleAuxRoot;
import ac.soton.codegen.core.ScheduleAttributes;
import ac.soton.codegen.core.plugin.CodegenPlugin;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;


public class ScheduleModule extends SCProcessorModule {

	private final IModuleType<ScheduleModule> MODULE_TYPE = SCCore.getModuleType(CodegenPlugin.PLUGIN_ID + ".scheduleModule");

	public final static int LABEL_SYMTAB_SIZE = 2047;
	public final static int IDENT_SYMTAB_SIZE = 2047;


	@Override
	public void process(IRodinElement element, IInternalElement target,
			ISCStateRepository repository, IProgressMonitor monitor)
			throws CoreException {
		monitor.subTask("ProcessingBound");

		final IRodinFile machineFile = (IRodinFile) element;
		final IScheduleAuxRoot machineRoot = (IScheduleAuxRoot) machineFile.getRoot();
		machineRoot.getFormulaFactory();
		final ISchedule[] scheds = machineRoot.getChildrenOfType(ISchedule.ELEMENT_TYPE);


		// ****************************************************************
		// ****************************************************************
		// A number of static checks can be implemented here in the future
		// ****************************************************************
		// ****************************************************************
		
		if(scheds.length>0){
			for(int i=0; i<scheds.length; i++){
				ISCSchedule scSchedule = (ISCSchedule) target.getInternalElement(ISCSchedule.ELEMENT_TYPE, ""); 
				scSchedule.create(null, monitor);		
				scSchedule.setSource(scheds[i], monitor);
				scSchedule.setAttributeValue(ScheduleAttributes.SCHEDULE_NAME_ATTRIBUTE, scheds[i].getScheduleName() , monitor);
				scSchedule.setAttributeValue(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE, scheds[i].getMachineName() , monitor);
				if(scheds[i].hasAttribute(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE))
					scSchedule.setAttributeValue(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE, scheds[i].getScheduleRefine() , monitor);
			}	
		}
	}

	@Override
	public IModuleType<?> getModuleType() {
		return MODULE_TYPE;
	}

}
