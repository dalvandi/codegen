package ac.soton.codegen.core.sc.modules;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.sc.SCCore;
import org.eventb.core.sc.SCProcessorModule;
import org.eventb.core.sc.state.ISCStateRepository;
import org.eventb.core.tool.IModuleType;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IRodinElement;

import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.plugin.CodegenPlugin;
import ac.soton.codegen.core.sc.WorkspaceManager;
import ac.soton.codegen.core.sc.states.ScheduleAuxAccuracyInfo;

public class ScheduleAuxModule extends SCProcessorModule {

	private final IModuleType<ScheduleAuxModule> MODULE_TYPE = SCCore.getModuleType(CodegenPlugin.PLUGIN_ID + ".scheduleAuxModule");

	public final static int LABEL_SYMTAB_SIZE = 2047;
	public final static int IDENT_SYMTAB_SIZE = 2047;

	private ScheduleAuxAccuracyInfo accuracyInfo;
	private ISCScheduleAuxRoot theoryRoot;
	private IRodinElement source;

	@SuppressWarnings("restriction")
	@Override
	public void endModule(IRodinElement element, ISCStateRepository repository, IProgressMonitor monitor) throws CoreException {
		theoryRoot.setAccuracy(accuracyInfo.isAccurate(), monitor);
		theoryRoot.setSource(source, monitor);
		endProcessorModules(element, repository, monitor);
		
		fireSCChange();
	}
	
	private void fireSCChange() throws CoreException {
		final String tmpName = theoryRoot.getRodinFile().getElementName();
		final int last = tmpName.lastIndexOf("_tmp");
		final String scName = last < 0 ? tmpName : tmpName.substring(0, last);
		final ISCScheduleAuxRoot scRoot = (ISCScheduleAuxRoot) theoryRoot
				.getRodinProject().getRodinFile(scName).getRoot();

		WorkspaceManager.getInstance().scScheduleAuxChanged(scRoot);
	}

	public IModuleType<?> getModuleType() {
		return MODULE_TYPE;
	}

	@Override
	public void initModule(IRodinElement element, ISCStateRepository repository, IProgressMonitor monitor) throws CoreException {
		accuracyInfo = new ScheduleAuxAccuracyInfo();
		
		repository.setState(accuracyInfo);
		initProcessorModules(element, repository, monitor);
	}

	@Override
	public void process(IRodinElement element, IInternalElement target, ISCStateRepository repository, IProgressMonitor monitor) 
	throws CoreException {
		theoryRoot = (ISCScheduleAuxRoot) target;
		source = element;
		processModules(element, target, repository, monitor);
	}

}
