package ac.soton.codegen.core.sc.modules;

import org.eventb.core.sc.SCCore;
import org.eventb.core.sc.state.IAccuracyInfo;
import org.eventb.core.tool.IStateType;
import org.eventb.internal.core.sc.AccuracyInfo;

import ac.soton.codegen.core.plugin.CodegenPlugin;

@SuppressWarnings("restriction")
public class ScheduleAccuracyInfo extends AccuracyInfo implements IAccuracyInfo{
	
	public final static IStateType<ScheduleAccuracyInfo> STATE_TYPE = SCCore
		.getToolStateType(CodegenPlugin.PLUGIN_ID + ".theoryAccuracyInfo");
	
	public IStateType<?> getStateType() {
		return STATE_TYPE;
	}

	// Re-declaring this method here avoids propagating warnings about
	// restricted access all over the code
	@Override
	public void setNotAccurate() {
		super.setNotAccurate();
	}

}