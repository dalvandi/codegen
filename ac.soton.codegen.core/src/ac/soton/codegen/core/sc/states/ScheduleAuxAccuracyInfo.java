package ac.soton.codegen.core.sc.states;

import org.eventb.core.sc.SCCore;
import org.eventb.core.sc.state.IAccuracyInfo;
import org.eventb.core.tool.IStateType;
import org.eventb.internal.core.sc.AccuracyInfo;

import ac.soton.codegen.core.plugin.CodegenPlugin;

@SuppressWarnings("restriction")
public class ScheduleAuxAccuracyInfo extends AccuracyInfo implements IAccuracyInfo{

	public final static IStateType<ScheduleAuxAccuracyInfo> STATE_TYPE = SCCore
			.getToolStateType(CodegenPlugin.PLUGIN_ID + ".scheduleAuxAccuracyInfo");
		
		public IStateType<?> getStateType() {
			return STATE_TYPE;
		}

		// Re-declaring this method here avoids propagating warnings about
		// restricted access all over the code
		@Override
		public void setNotAccurate() {
			super.setNotAccurate();
		}
}
