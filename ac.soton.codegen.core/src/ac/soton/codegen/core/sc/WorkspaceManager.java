package ac.soton.codegen.core.sc;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.core.runtime.CoreException;
import org.rodinp.core.ElementChangedEvent;
import org.rodinp.core.IElementChangedListener;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinElementDelta;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinCore;

import ac.soton.codegen.core.ISCScheduleAuxRoot;

public class WorkspaceManager implements IElementChangedListener {

	private static final WorkspaceManager INSTANCE = new WorkspaceManager();
	private final Queue<ISCScheduleAuxRoot> changedSC = new ConcurrentLinkedQueue<ISCScheduleAuxRoot>();

	private WorkspaceManager() {
		RodinCore.addElementChangedListener(this);
		//initDeployedGraph();
	}
	
	public static WorkspaceManager getInstance() {
		return INSTANCE;
	}

	@Override
	public void elementChanged(ElementChangedEvent event) {
		final IRodinElementDelta delta = event.getDelta();
		processDelta(delta);
	}

	@SuppressWarnings("unused")
	private void processDelta(IRodinElementDelta delta) {
		IRodinElement element = delta.getElement();
		IRodinElementDelta[] affected = delta.getAffectedChildren();

		if (element instanceof IRodinFile) {
			final IRodinFile file = (IRodinFile) element;
		} else {
			for (IRodinElementDelta d : affected) {
				processDelta(d);
			}
		}
	}

	
	public void scScheduleAuxChanged(ISCScheduleAuxRoot scRoot) throws CoreException {
		changedSC.add(scRoot);
	}

}
