package ac.soton.codegen.core.sc;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.sc.StaticChecker;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinCore;
import org.rodinp.core.builder.IGraph;
import org.eventb.internal.core.sc.Messages;

import ac.soton.codegen.core.IScheduleAuxRoot;

@SuppressWarnings("restriction")
public class ScheduleStaticChecker extends StaticChecker{

	@Override
	public void extract(IFile file, IGraph graph, IProgressMonitor monitor) throws CoreException {
		// TODO Auto-generated method stub
		System.out.println("Schedule Static Checker :: " + file.getName());
		try {
			monitor.beginTask(Messages.bind(Messages.build_extracting, file.getName()), 1);
			IRodinFile source = RodinCore.valueOf(file);
			IScheduleAuxRoot root = (IScheduleAuxRoot) source.getRoot();
			IRodinFile target = root.getSCScheduleAuxRoot().getRodinFile();
			graph.addTarget(target.getResource());
			graph.addToolDependency(source.getResource(), target.getResource(), true);

		}
		finally {
			monitor.done();
		}
	}
	

}
