package ac.soton.codegen.core;

import org.eclipse.core.runtime.IProgressMonitor;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface IEvt extends IInternalElement{
	
	IInternalElementType<ISchedule> ELEMENT_TYPE = 
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".event");

	public void setEventName(String name, IProgressMonitor monitor) throws RodinDBException;
	public String getEventName() throws RodinDBException;

}
