package ac.soton.codegen.core;

import org.eventb.core.IGeneratedElement;
import org.eventb.core.ILabeledElement;
import org.eventb.core.ISCPredicateElement;
import org.eventb.core.ITraceableElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface ISCSchedule extends ILabeledElement, ISCPredicateElement, ITraceableElement,
IGeneratedElement{
	
	IInternalElementType<ISCSchedule> ELEMENT_TYPE =
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".scSchedule");

}
