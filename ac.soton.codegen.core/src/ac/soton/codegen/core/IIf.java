package ac.soton.codegen.core;

import org.eclipse.core.runtime.IProgressMonitor;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface IIf  extends IInternalElement{
	IInternalElementType<IIf> ELEMENT_TYPE = 
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".if");

	public void setCond(String cond, IProgressMonitor monitor) throws RodinDBException;
	public String getCond() throws RodinDBException;

}
