package ac.soton.codegen.core;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.basis.PORoot;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinDBException;

public class ScheduleIPORoot extends PORoot{

	public ScheduleIPORoot(String name, IRodinElement parent) {
		super(name, parent);
	}
	public boolean exists() {
		return false;
	}

	public void create(IInternalElement nextSibling, IProgressMonitor monitor) throws RodinDBException {
		IRodinFile file = nextSibling.getRodinProject().getRodinFile(nextSibling.getElementName() + ".bpo");
		file.create(true, null);
	}

}
