package ac.soton.codegen.core;

import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface IIteration extends IInternalElement{
	
	IInternalElementType<IIteration> ELEMENT_TYPE = 
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".iteration");


}
