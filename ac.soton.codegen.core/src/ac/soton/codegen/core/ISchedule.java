package ac.soton.codegen.core;

import org.eclipse.core.runtime.IProgressMonitor;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface ISchedule extends IInternalElement{
	IInternalElementType<ISchedule> ELEMENT_TYPE = 
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".schedule");

	public boolean hasScheduleRefine() throws RodinDBException;
	public void setScheduleName(String name, IProgressMonitor monitor) throws RodinDBException;
	public void setScheduleRefine(String refine, IProgressMonitor monitor) throws RodinDBException;
	public void setMachineName(String name, IProgressMonitor monitor) throws RodinDBException;
	
	public String getScheduleName() throws RodinDBException;
	public String getScheduleRefine() throws RodinDBException;
	public String getMachineName() throws RodinDBException;
}
