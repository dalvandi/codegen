package ac.soton.codegen.core.pogen;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.IPOPredicateSet;
import org.eventb.core.IPORoot;
import org.eventb.core.IPOSource;
import org.eventb.core.ISCMachineRoot;
import org.eventb.core.ast.FormulaFactory;
import org.eventb.core.ast.IParseResult;
import org.eventb.core.ast.ITypeEnvironment;
import org.eventb.core.ast.Predicate;
import org.eventb.core.pog.IPOGHint;
import org.eventb.core.pog.IPOGPredicate;
import org.eventb.core.pog.IPOGSource;
import org.eventb.core.pog.POGCore;
import org.eventb.core.pog.POGProcessorModule;
import org.eventb.core.pog.state.IPOGStateRepository;
import org.eventb.core.tool.IModuleType;
import org.rodinp.core.IRodinDB;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import ac.soton.codegen.core.ISCSchedule;
import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.ScheduleAttributes;
import ac.soton.codegen.core.plugin.CodegenPlugin;

public class POGenerator extends POGProcessorModule{
	
	private static final IModuleType<POGenerator> MODULE_TYPE = POGCore
			.getModuleType(CodegenPlugin.PLUGIN_ID
					+ ".poGenerator");

	@SuppressWarnings("unused")
	@Override
	public void process(IRodinElement element, IPOGStateRepository repository, IProgressMonitor monitor)
			throws CoreException {
		// TODO Auto-generated method stub
		System.out.println("PO GENERATION");
		FormulaFactory ff =  repository.getFormulaFactory();
		ITypeEnvironment typeEnvironment;
		IRodinFile scheduleFile = (IRodinFile) element;
		ISCScheduleAuxRoot scheduleRoot = (ISCScheduleAuxRoot) scheduleFile.getRoot();
		//if(scheduleRoot.exists()) {
			ISCSchedule[] schedules = scheduleRoot.getChildrenOfType(ISCSchedule.ELEMENT_TYPE);
			ISCSchedule schedule = schedules[0];
			String machineName = schedule.getAttributeValue(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE);
			
			
			IRodinDB rodinDB = RodinCore.getRodinDB();
			IRodinProject prj = element.getRodinProject();
			IRodinFile machineFile = prj.getRodinFile(machineName + ".bcm");
			ISCMachineRoot machineRoot = (ISCMachineRoot) machineFile.getRoot();
			typeEnvironment = machineRoot.getTypeEnvironment();
			ff =  machineRoot.getFormulaFactory();
			System.out.println("SCHEDULE INFO: " + machineName);
		//}
		
		final IPORoot target = repository.getTarget();
		IPOPredicateSet hyp = target
				.getPredicateSet("ABSHYP");
		String poName = "testPO";
		
				
		List<IPOGPredicate> localHypothesis = new ArrayList<IPOGPredicate>();
		
		IParseResult h1 = ff.parsePredicate("a = 1", null);
		IParseResult h2 = ff.parsePredicate("b = 2", null);
		Predicate predicateh1 =  h1.getParsedPredicate();
		Predicate predicateh2 =  h2.getParsedPredicate();
		localHypothesis.add(makePredicate(predicateh1, element));
		localHypothesis.add(makePredicate(predicateh2, element));
		
		IParseResult hyporesult = ff.parsePredicate("a = b", null);
		 
		Predicate predicate =  hyporesult.getParsedPredicate();
		predicate.typeCheck(typeEnvironment);
		 
			final IPOGSource[] sources = new IPOGSource[] {makeSource(IPOSource.DEFAULT_ROLE, element),
					makeSource(IPOSource.DEFAULT_ROLE, element)};
		createPO(target,
				poName,
				POGProcessorModule.makeNature("Test_Proof_Obligation"),
				hyp,
				localHypothesis,
				makePredicate(predicate,element),
				sources,
				new IPOGHint[] {
						makeIntervalSelectionHint(hyp, getSequentHypothesis(target, poName))
				},
				true,
				monitor);
		
		
		
	}

	@Override
	public IModuleType<?> getModuleType() {
		// TODO Auto-generated method stub
		return MODULE_TYPE;
	}
	
	@Override
	public void initModule(IRodinElement element,
			IPOGStateRepository repository, IProgressMonitor monitor)
			throws CoreException {
		System.out.println("Proof Obligation Generator started:\n\n");

	}

}
