package ac.soton.codegen.core.pogen.modules;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.pog.POGCore;
import org.eventb.core.pog.state.IPOGStateRepository;
import org.eventb.core.tool.IModuleType;
import org.eventb.internal.core.pog.modules.BaseModule;
import org.rodinp.core.IRodinElement;

import ac.soton.codegen.core.plugin.CodegenPlugin;

@SuppressWarnings("restriction")
public class ScheduleAuxPOGModule extends BaseModule {

	private final IModuleType<ScheduleAuxPOGModule> MODULE_TYPE = 
		POGCore.getModuleType(CodegenPlugin.PLUGIN_ID + ".scheduleAuxPOGModule"); 
	
	@Override
	public void process(
			IRodinElement element, 
			IPOGStateRepository repository, 
			IProgressMonitor monitor) throws CoreException {
		processModules(element, repository, monitor);
	}
	
	public IModuleType<?> getModuleType() {
		return MODULE_TYPE;
	}

}