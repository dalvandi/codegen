package ac.soton.codegen.core;

import org.rodinp.core.IAttributeType;
import org.rodinp.core.RodinCore;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public class ScheduleAttributes {

	public static IAttributeType.String SCHEDULE_NAME_ATTRIBUTE = RodinCore
			.getStringAttrType(CodegenPlugin.PLUGIN_ID + ".scheduleName");

	public static IAttributeType.String SCHEDULE_REFINE_ATTRIBUTE = RodinCore
			.getStringAttrType(CodegenPlugin.PLUGIN_ID + ".scheduleRefine");

	public static IAttributeType.String MACHINE_NAME_ATTRIBUTE = RodinCore
			.getStringAttrType(CodegenPlugin.PLUGIN_ID + ".machineName");

	public static IAttributeType.String EVENT_NAME_ATTRIBUTE = RodinCore
			.getStringAttrType(CodegenPlugin.PLUGIN_ID + ".eventName");

	public static IAttributeType.String COND_ATTRIBUTE = RodinCore
			.getStringAttrType(CodegenPlugin.PLUGIN_ID + ".cond");

}
