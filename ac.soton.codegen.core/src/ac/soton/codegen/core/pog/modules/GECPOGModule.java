package ac.soton.codegen.core.pog.modules;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.IPOIdentifier;
import org.eventb.core.IPOPredicateSet;
import org.eventb.core.IPORoot;
import org.eventb.core.ISCMachineRoot;
import org.eventb.core.ISCVariable;
import org.eventb.core.ast.FormulaFactory;
import org.eventb.core.ast.ITypeEnvironmentBuilder;
import org.eventb.core.ast.IntegerType;
import org.eventb.core.pog.POGCore;
import org.eventb.core.pog.POGProcessorModule;
import org.eventb.core.pog.state.IPOGStateRepository;
import org.eventb.core.tool.IModuleType;
import org.rodinp.core.IRodinDB;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.ISCSchedule;
import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.ScheduleAttributes;
import ac.soton.codegen.core.plugin.CodegenPlugin;
@SuppressWarnings("unused")
public class GECPOGModule extends POGProcessorModule {

	public GECPOGModule() {
		// TODO Auto-generated constructor stub
	}

	public static final String ABS_HYP_NAME = "ABSHYP";
	
	private final IModuleType<GECPOGModule> MODULE_TYPE = 
		POGCore.getModuleType(CodegenPlugin.PLUGIN_ID + ".gecPOGModule"); //$NON-NLS-1$
	
	@Override
	public IModuleType<?> getModuleType() {
		return MODULE_TYPE;
	}
	
	private IPORoot target;
	private ITypeEnvironmentBuilder typeEnvironment;
	private FormulaFactory factory;
	
	@Override
	public void process(IRodinElement element, IPOGStateRepository repository,
			IProgressMonitor monitor) throws CoreException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initModule(
			IRodinElement element, 
			IPOGStateRepository repository,
			IProgressMonitor monitor) throws CoreException {
		super.initModule(element, repository, monitor);
		factory = repository.getFormulaFactory();
		typeEnvironment = repository.getTypeEnvironment();
		
		IRodinFile scScheduleAuxFile = (IRodinFile) element;
		ISCScheduleAuxRoot scScheduleAuxRoot = (ISCScheduleAuxRoot) scScheduleAuxFile.getRoot();
		target = repository.getTarget();
		
		IPOPredicateSet rootSet = target.getPredicateSet(ABS_HYP_NAME);
		rootSet.create(null, monitor);
		
		addIdentifiers(element, rootSet);
		
	}

	private void addIdentifiers(IRodinElement element, IPOPredicateSet rootSet) throws CoreException {
		
		IRodinFile scheduleFile = (IRodinFile) element;
		ISCScheduleAuxRoot scheduleRoot = (ISCScheduleAuxRoot) scheduleFile.getRoot();
		ISCSchedule[] schedules = scheduleRoot.getChildrenOfType(ISCSchedule.ELEMENT_TYPE);
		ISCSchedule schedule = schedules[0];
		String machineName = schedule.getAttributeValue(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE);
		IRodinDB rodinDB = RodinCore.getRodinDB();
		IRodinProject prj = element.getRodinProject();
		IRodinFile machineFile = prj.getRodinFile(machineName + ".bcm");
		ISCMachineRoot machineRoot = (ISCMachineRoot) machineFile.getRoot();
		FormulaFactory ff =  machineRoot.getFormulaFactory();

		for(ISCVariable v : machineRoot.getSCVariables())
		{
			IPOIdentifier poIdentifier = rootSet.getIdentifier(v.getElementName());
			poIdentifier.create(null, null);
			poIdentifier.setType(v.getType(ff), null);
		}
		
	}

	@Override
	public void endModule(
			IRodinElement element, 
			IPOGStateRepository repository,
			IProgressMonitor monitor) throws CoreException {
		target = null;
		typeEnvironment = null;
		factory = null;
		super.endModule(element, repository, monitor);
	}


}
