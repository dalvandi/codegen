package ac.soton.codegen.core.pog;

import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.pog.ProofObligationGenerator;
import org.rodinp.core.builder.IGraph;

import ac.soton.codegen.core.ISCScheduleAuxRoot;
import ac.soton.codegen.core.sc.Messages;

import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinCore;

public class ScheduleProofObligationGenerator extends ProofObligationGenerator{

	public void extract(IFile file, IGraph graph, IProgressMonitor monitor)
			throws CoreException {
		System.out.println("Proof Obligation Generator tool :: " + file.getName());
		try {
			monitor.beginTask(
					MessageFormat.format(Messages.build_extracting, file.getName()), 1);
			IRodinFile source = RodinCore.valueOf(file);
			ISCScheduleAuxRoot root = (ISCScheduleAuxRoot) source.getRoot();
			IRodinFile target = root.getPORoot().getRodinFile();
			graph.addTarget(target.getResource());
			graph.addToolDependency(source.getResource(), target.getResource(),
					true);
		} finally {
			monitor.done();
		}
	}
}
