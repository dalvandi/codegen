package ac.soton.codegen.core;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.IAccuracyElement;
import org.eventb.core.IConfigurationElement;
import org.eventb.core.IEventBRoot;
import org.eventb.core.ITraceableElement;
import org.eventb.core.ast.FormulaFactory;
import org.eventb.core.ast.ITypeEnvironment;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface ISCScheduleRoot extends IEventBRoot, IAccuracyElement, IConfigurationElement, 
ITraceableElement,IInternalElement{
	
	IInternalElementType<ISCScheduleRoot> ELEMENT_TYPE = RodinCore
			.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".scScheduleRoot");

	ITypeEnvironment getTypeEnvironment(FormulaFactory factory) throws RodinDBException;
	IRodinFile getScheduleFile(String bareName);
	IScheduleRoot getScheduleRoot();
	IScheduleRoot getScheduleRoot(String bareName);
	IRodinFile getRodinFile();
	ScheduleIPORoot getSchedulePORoot(IProgressMonitor monitor);

}
