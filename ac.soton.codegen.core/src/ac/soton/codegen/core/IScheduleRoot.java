package ac.soton.codegen.core;

import org.eventb.core.ICommentedElement;
import org.eventb.core.IConfigurationElement;
import org.eventb.core.IEventBRoot;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.RodinCore;
import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface IScheduleRoot extends 
IEventBRoot,ICommentedElement, IConfigurationElement{
	IInternalElementType<IScheduleRoot> ELEMENT_TYPE = RodinCore
			.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".scheduleRoot");
	
	IRodinFile getSCScheduleFile(String bareName);
	
	ISCScheduleRoot getSCScheduleRoot();
	
	ISCScheduleRoot getSCScheduleRoot(String bareName);

}
