package ac.soton.codegen.core.plugin;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;
//import org.rodinp.core.RodinCore;

//import ac.soton.codegen.core.ConfSettor;


public class CodegenPlugin extends Plugin {
	
	private static BundleContext context;
	public static final String PLUGIN_ID = "ac.soton.codegen.core";

	static BundleContext getContext() {
		return context;
	}


	@Override
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("Plugin started...");
		CodegenPlugin.context = bundleContext;
		//setProbConfig();
	}

	@Override
	public void stop(BundleContext arg0) throws Exception {
		CodegenPlugin.context = null;

	}
	
	public static void setProbConfig() {
		//RodinCore.addElementChangedListener(new ConfSettor());
	}

}
