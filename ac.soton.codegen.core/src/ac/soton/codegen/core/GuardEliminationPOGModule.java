package ac.soton.codegen.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eventb.core.pog.POGCore;
import org.eventb.core.pog.POGProcessorModule;
import org.eventb.core.pog.state.IPOGStateRepository;
import org.eventb.core.tool.IModuleType;
import org.rodinp.core.IRodinElement;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public class GuardEliminationPOGModule extends POGProcessorModule {

	private final IModuleType<GuardEliminationPOGModule> MODULE_TYPE = 
			POGCore.getModuleType(CodegenPlugin.PLUGIN_ID + ".guardEliminationPOGModule"); //$NON-NLS-1$

	public GuardEliminationPOGModule() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void process(IRodinElement element, IPOGStateRepository repository, IProgressMonitor monitor)
			throws CoreException {
		// TODO Auto-generated method stub
		System.out.println("GuardEliminationPOGModule");
	}

	@Override
	public IModuleType<?> getModuleType() {
		// TODO Auto-generated method stub
		return MODULE_TYPE;
	}

}
