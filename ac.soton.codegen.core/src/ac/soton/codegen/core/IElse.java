package ac.soton.codegen.core;

import org.rodinp.core.IInternalElement;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.RodinCore;

import ac.soton.codegen.core.plugin.CodegenPlugin;

public interface IElse extends IInternalElement{
	IInternalElementType<IElse> ELEMENT_TYPE = 
			RodinCore.getInternalElementType(CodegenPlugin.PLUGIN_ID + ".else");

}
