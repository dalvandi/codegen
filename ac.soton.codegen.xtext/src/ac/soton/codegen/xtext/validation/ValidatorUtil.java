package ac.soton.codegen.xtext.validation;

import org.rodinp.core.IRodinDB;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.xtext.sEB.Schedule;

public class ValidatorUtil {

	public static boolean machineExists(Schedule sched) throws RodinDBException 
	{
		String machineName = sched.getMachineName();
		String proj = sched.eResource().getURI().segmentsList().get(1);
		IRodinDB rodinDB = RodinCore.getRodinDB();
		IRodinProject rodinProject = rodinDB.getRodinProject(proj);
		IRodinFile[] rodinFile = rodinProject.getRodinFiles();
		
		boolean exists = false;
		for(IRodinFile f : rodinFile)
		{
			if(f.toString().equals(machineName + ".bcm"))
			{	
				
				exists = true;
			}
		}
		
		return exists;
	} 
	
	public static boolean isPredicate(Schedule sched, String pred)
	{
		
		return false;
	}
}
