package ac.soton.codegen.xtext.generator;

public class LoopNode extends Node{
	String cond;
	BodyNode body;
	
	
	public void setCond(String str)
	{
		cond = str;
	}
	
	public void setBody(BodyNode node)
	{
		body = node;
	}
	
	public String getCond()
	{
		return cond;
	}
	
	public BodyNode getBody()
	{
		return body;
	}
	

	
	
}
