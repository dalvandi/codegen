package ac.soton.codegen.xtext.generator;

import java.util.ArrayList;

public class ChoiceNode extends Node {
	
	ArrayList<Node> choices;
	
	public ChoiceNode() {
		choices = new ArrayList<Node>();
	}
	
	public void addChoice(Node choice)
	{
		choices.add(choice);
	}
	
	
	public ArrayList<Node> getChoices()
	{
		return choices;
	}
	

}
