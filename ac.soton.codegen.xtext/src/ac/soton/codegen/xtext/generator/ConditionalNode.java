package ac.soton.codegen.xtext.generator;

import java.util.ArrayList;

public class ConditionalNode extends Node{

	private String ifCond;
	private BodyNode ifBody;
	private ArrayList<String> ifelseConds;
	private ArrayList<BodyNode> ifelseBodies;
	private BodyNode elseBody;
	
	
	public ConditionalNode()
	{
		ifelseConds = new ArrayList<String>();
		ifelseBodies = new ArrayList<BodyNode>();
	}
	
	public void setIf(String cond, BodyNode body)
	{
		this.ifCond = cond;
		this.ifBody = body;
	}
	
	public void setElse(BodyNode body)
	{
		this.elseBody = body;
	}
	
	public void addElseIf(String cond, BodyNode body)
	{
		this.ifelseConds.add(cond);
		this.ifelseBodies.add(body);
	}
	
	public String getIfCond() {
		return ifCond;
	}

	public BodyNode getIfBody() {
		return ifBody;
	}

	public ArrayList<String> getIfelseConds() {
		return ifelseConds;
	}

	public ArrayList<BodyNode> getIfelseBodies() {
		return ifelseBodies;
	}

	public BodyNode getElseBody() {
		return elseBody;
	}


	
	
}
