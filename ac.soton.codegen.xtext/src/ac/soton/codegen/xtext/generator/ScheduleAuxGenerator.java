package ac.soton.codegen.xtext.generator;

import java.util.ArrayList;

import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eventb.core.IConfigurationElement;
import org.rodinp.core.IInternalElement;
import org.rodinp.core.IRodinDB;
import org.rodinp.core.IRodinFile;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IChoice;
import ac.soton.codegen.core.IEvt;
import ac.soton.codegen.core.IIteration;
import ac.soton.codegen.core.ILoop;
import ac.soton.codegen.core.ISchedule;
import ac.soton.codegen.core.ScheduleAttributes;
import ac.soton.codegen.xtext.sEB.BooleanExpression;
import ac.soton.codegen.xtext.sEB.Choice;
import ac.soton.codegen.xtext.sEB.Conditional;
import ac.soton.codegen.xtext.sEB.Event;
import ac.soton.codegen.xtext.sEB.Expression;
import ac.soton.codegen.xtext.sEB.Iteration;
import ac.soton.codegen.xtext.sEB.Loop;
import ac.soton.codegen.xtext.sEB.Schedule;
import ac.soton.codegen.xtext.sEB.ScheduleBody;

public  class ScheduleAuxGenerator {
	public static ArrayList<ScheduleBody> schedule = new ArrayList<ScheduleBody>();

	public static boolean createScheduleAux(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context)
	{
		String proj = resource.getURI().segmentsList().get(1);
		IRodinDB rodinDB = RodinCore.getRodinDB();
		IRodinProject rodinProject = rodinDB.getRodinProject(proj);
		printElements(resource);
		if(!rodinProject.exists())
			return false;
			
		try {
			RodinCore.run(new IWorkspaceRunnable() {
				public void run(IProgressMonitor pMonitor) throws CoreException {
					
					String shcedulefileName = resource.getURI().lastSegment();
					String fileName = shcedulefileName.replaceAll(".seb", ".sax");
					System.out.println(fileName);
					String machineName = "";
					String scheduleName = "";
					String refineSchedule = "";
					
					TreeIterator<EObject> itr = resource.getAllContents(); 
					Schedule sched;
					while(itr.hasNext())
					{
						EObject e = itr.next();
						if(e instanceof Schedule) {
							sched = (Schedule) e;
							machineName = sched.getMachineName();
							scheduleName = sched.getShedName() ;
							refineSchedule = sched.getRefinedShed();
							/*System.out.println("Schedule" + " :: name: " 
									+  scheduleName
									+ " machine: " + machineName
									+ " refine: " + refineSchedule);*/
						
						}
						else if(e instanceof ScheduleBody) {
								break;
						}
					}

					
					ScheduleBody sbroot = ((Schedule)(resource.getContents().get(0))).getSheds().get(0);
					BodyNode bn = new BodyNode();
					generate(sbroot, bn);

					final IRodinFile rodinFile = rodinProject
							.getRodinFile(fileName);
					if(!rodinFile.exists())
						rodinFile.create(false, pMonitor);
					
					final IInternalElement rodinRoot = rodinFile.getRoot();
					((IConfigurationElement) rodinRoot).setConfiguration(
							"ac.soton.codegen.core.schedConfig", pMonitor);
					final ISchedule rodinEl = rodinRoot.getInternalElement(ISchedule.ELEMENT_TYPE, scheduleName);
					
					if(rodinEl.exists())
					rodinEl.clear(true, null);
					
					
					//addElements(rodinEl, bn);
					
					
					if(!rodinEl.exists())
						rodinEl.create(null, null);
					else
					{
						rodinEl.removeAttribute(ScheduleAttributes.MACHINE_NAME_ATTRIBUTE, null);
						rodinEl.removeAttribute(ScheduleAttributes.SCHEDULE_NAME_ATTRIBUTE, null);
						rodinEl.removeAttribute(ScheduleAttributes.SCHEDULE_REFINE_ATTRIBUTE, null);
					}
					rodinEl.setMachineName(machineName, null);
					rodinEl.setScheduleName(scheduleName, null);
					if(refineSchedule!=null)
						rodinEl.setScheduleRefine(refineSchedule, null);
					
					addElements(rodinEl, bn);

					rodinFile.save(null, true);

				}

				private void addElements(IInternalElement rodinEl, Node node) throws RodinDBException {
					
					if(node instanceof BodyNode)
					{
						for(Node n : ((BodyNode) node).getChildren())
						{
							addElements(rodinEl, n);
						}
					}
					else if(node instanceof ConditionalNode)
					{
/*						System.out.print("if(" + ((ConditionalNode) node).getIfCond() + "){");
						printNode(((ConditionalNode) node).getIfBody());
						System.out.print("}");
						
						for(int i = 0; i < ((ConditionalNode) node).getIfelseConds().size(); i++)
						{
							String cond = ((ConditionalNode) node).getIfelseConds().get(i);
							BodyNode bn = ((ConditionalNode) node).getIfelseBodies().get(i);
							System.out.print("else if(" + cond + "){");
							printNode(bn);
							System.out.print("}");			
						}
						
*/					}
					else if(node instanceof LoopNode)
					{
						final ILoop loop = (ILoop) rodinEl.getInternalElement(ILoop.ELEMENT_TYPE, node.toString());
	
						if(!loop.exists())
							loop.create(null, null);
						
						loop.setCond(((LoopNode) node).getCond(), null);
						
						addElements(loop, ((LoopNode) node).getBody());

					}
					else if(node instanceof IterationNode)
					{
						final IIteration itr = (IIteration) rodinEl.getInternalElement(IIteration.ELEMENT_TYPE, node.toString());
						if(!itr.exists())
							itr.create(null, null);
						
						addElements(itr, ((IterationNode) node).getBody());
						
					}
					else if(node instanceof ChoiceNode)
					{
						final IChoice choice = (IChoice) rodinEl.getInternalElement(IChoice.ELEMENT_TYPE, node.toString());
						if(!choice.exists())
							choice.create(null, null);
						for(Node n : ((ChoiceNode) node).getChoices()) {
							addElements(choice, n);
						}

					}
					else if(node instanceof EventNode)
					{
						final IEvt evtEl = (IEvt) rodinEl.getInternalElement(IEvt.ELEMENT_TYPE, ((EventNode) node).getEventName());
						
						if(!evtEl.exists())
							evtEl.create(null, null);
						
						evtEl.setEventName(((EventNode) node).getEventName(), null);
					}
				}
				
			}, null);
		} catch (RodinDBException e) {
			e.printStackTrace();
		}

		return false;
	}

	
	
	public static void generate(ScheduleBody schedBody, BodyNode parent) {
		
		parent.addChild(getNode(schedBody.getSleft()));
		if(schedBody.getSright() != null) {
			generate(schedBody.getSright(), parent);
		}
		
	}
	
	
	private static Node getNode(Expression e) {
		if(e instanceof Event)
		{
			EventNode en = new EventNode();
			en.setEventName(((Event) e).getName());
			return en;
		}
		else if(e instanceof Iteration)
		{
			IterationNode itrn = new IterationNode();
			BodyNode bn = new BodyNode();
			generate(((Iteration) e).getIterationBody(), bn);
			itrn.setBody(bn);
			return itrn;
		}
		else if(e instanceof Loop) {
			LoopNode ln = new LoopNode();
			BodyNode lb = new BodyNode();
			generate(((Loop) e).getBody(), lb);
			ln.setBody(lb);
			ln.setCond(((Loop) e).getCondition().getPredicate());
			return ln;
		}
		else if(e instanceof Conditional)
		{
			ConditionalNode cn = new ConditionalNode();
			BodyNode ifbody = new BodyNode();
			generate(((Conditional) e).getThen(),ifbody);
			cn.setIf(((Conditional) e).getCondition().getPredicate(), ifbody);
			
			for(int i = 0; i < ((Conditional) e).getElseCond().size(); i++)
			{
				BodyNode elseifbody = new BodyNode();
				generate(((Conditional) e).getElseif().get(i), elseifbody);
				cn.addElseIf(((Conditional) e).getElseCond().get(i).getPredicate(), elseifbody);
			}
			
			if(((Conditional) e).getElse()!=null)
			{
				BodyNode elsebody = new BodyNode();
				generate(((Conditional) e).getElse(), elsebody);
				cn.setElse(elsebody);
			}
			return cn;
		}
		else if(e instanceof Choice)
		{
			ChoiceNode chn = new ChoiceNode();
			chn.addChoice(getNode(((Choice) e).getLefths()));
			for(Expression exp : ((Choice) e).getRighths())
			{
				chn.addChoice(getNode(exp));
			}
			
			return chn;
		}
		return null;
	}



	@SuppressWarnings("unused")
	private static String generateScheduleBody(EObject e) {
		
		if(e instanceof ScheduleBody)
			return generateScheduleBody(((ScheduleBody) e).getSleft()) + ";" 
			+ generateScheduleBody(((ScheduleBody) e).getSright());
		else if(e instanceof Event)
			return ((Event) e).getName();
		else if(e instanceof Choice)
		{
			String body = "[" + generateScheduleBody(((Choice) e).getLefths());
			
			for(Expression exp: ((Choice)e).getRighths())
			{
				body += "[]" + generateScheduleBody(exp);
			}
			
			body += "]";
			
			return body;
		}
		else if(e instanceof Iteration)
			return "(" + generateScheduleBody(((Iteration) e).getIterationBody()) + ")*";
		else if(e instanceof Conditional)
		{
			String body = "if(" + generateScheduleBody(((Conditional) e).getCondition()) +"){"
					+ generateScheduleBody(((Conditional) e).getThen()) + "}";
			
			for(int i = 0; i < ((Conditional)e).getElseCond().size(); i++)
			{
				body += "else if(" + generateScheduleBody(((Conditional) e).getElseCond().get(i)) +"){"
						+ generateScheduleBody(((Conditional) e).getElseif().get(i)) + "}";
			}
			
			if(((Conditional) e).getElse() != null)
			{
				body += "else{" + generateScheduleBody(((Conditional) e).getElse()) + "}";
			}
			
			return body;
		}
		else if(e instanceof BooleanExpression)
			return ((BooleanExpression) e).getPredicate();
		else if(e instanceof Loop)
			return "while(" + generateScheduleBody(((Loop)e).getCondition()) + "){"
					+ generateScheduleBody(((Loop)e).getBody()) + "}";
		return "";
	}
	
	
	
	public static ArrayList<Expression> printElements(Resource resource)
	{

		ArrayList<Expression> scheds = new ArrayList<Expression>();
		
		ScheduleBody sbroot = ((Schedule)(resource.getContents().get(0))).getSheds().get(0);
		generateScheduleBodyTopElements(sbroot, true, scheds);
		
		
		BodyNode root = new BodyNode();
		generate(sbroot, root);
		
		for(Node n : root.getChildren())
		{
			System.out.println(n.toString());
		}
		
		//printNode(root);
		
		
		return scheds;
	}
	
	
	public static void printNode(Node node)
	{
		if(node instanceof BodyNode)
		{
			for(Node n : ((BodyNode) node).getChildren())
			{
				printNode(n);
			}
		}
		else if(node instanceof ConditionalNode)
		{
			System.out.print("if(" + ((ConditionalNode) node).getIfCond() + "){");
			printNode(((ConditionalNode) node).getIfBody());
			System.out.print("}");
			
			for(int i = 0; i < ((ConditionalNode) node).getIfelseConds().size(); i++)
			{
				String cond = ((ConditionalNode) node).getIfelseConds().get(i);
				BodyNode bn = ((ConditionalNode) node).getIfelseBodies().get(i);
				System.out.print("else if(" + cond + "){");
				printNode(bn);
				System.out.print("}");			
			}
			
		}
		else if(node instanceof LoopNode)
		{
			System.out.print("while(" + ((LoopNode) node).getCond() + "){");
			printNode(((LoopNode) node).getBody());
			System.out.print("}");			
		}
		else if(node instanceof IterationNode)
		{
			System.out.print("(");
			printNode(((IterationNode) node).getBody());
			System.out.print(")*");
		}
		else if(node instanceof ChoiceNode)
		{
			System.out.print("[");
			for(Node n : ((ChoiceNode) node).getChoices())
			{
				printNode(n);
				System.out.print("[]");

			}
			System.out.print("]");

		}
		else if(node instanceof EventNode)
		{
			System.out.print(((EventNode) node).getEventName());
		}
	}
	
	
	public static void printElements(ArrayList<Expression> schedules)
	{
		int i = 0;
		for(Expression sb : schedules)
		{
			i++;
			System.out.println(i + "- " + sb.toString());

		}

	}
	
	
	
	public static void generateExpressions(Expression exp)
	{
		if(exp instanceof Event)
		{
			System.out.println(((Event) exp).getName());
		}
		else if(exp instanceof Conditional)
		{
			
		}
	}
	
	
	public static void generateScheduleBodyTopElements(EObject e, boolean top, ArrayList<Expression> scheds) {
		if(e instanceof ScheduleBody)
		{
			if(top)
				scheds.add(((ScheduleBody) e).getSleft());
			
			generateScheduleBodyTopElements(((ScheduleBody) e).getSleft(), top, scheds);
			generateScheduleBodyTopElements(((ScheduleBody) e).getSright(), top, scheds);
		}
		else if(e instanceof Conditional)
		{
			generateScheduleBodyTopElements(((Conditional) e).getCondition(), top, scheds);
			generateScheduleBodyTopElements(((Conditional) e).getThen(), false, scheds);
			for(int i = 0; i < ((Conditional)e).getElseCond().size(); i++)
			{
				generateScheduleBodyTopElements(((Conditional) e).getElseif().get(i), false, scheds);
			}
			generateScheduleBodyTopElements(((Conditional) e).getElse(), false, scheds);

		}
		else if(e instanceof Loop)
		{
			generateScheduleBodyTopElements(((Loop) e).getBody(), false, scheds);
		}
		else if(e instanceof Choice)
		{
			generateScheduleBodyTopElements(((Choice) e).getLefths(), false, scheds);
			for(Expression ee : ((Choice) e).getRighths())
			{
				generateScheduleBodyTopElements(ee, false, scheds);
			}
		}
		else if(e instanceof Iteration)
		{
			generateScheduleBodyTopElements(((Iteration) e).getIterationBody(), false, scheds);
		}

	}
	
	
	public boolean isConcrete()
	{
		
		return true;
	}
}
