package ac.soton.codegen.xtext.generator;


public class IterationNode extends Node {
	private BodyNode body;
	
	public void setBody(BodyNode b)
	{
		body = b;
	}
	
	public BodyNode getBody()
	{
		return body;
	}
	
	
	
}
