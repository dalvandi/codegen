package ac.soton.codegen.xtext.generator;

import java.util.ArrayList;

public class BodyNode extends Node {
	private ArrayList<Node> children;
	
	public BodyNode()
	{
		children = new ArrayList<Node>();
	}
	
	
	public void addChild(Node node)
	{
		children.add(node);
	}
	
	
	public ArrayList<Node> getChildren()
	{
		return children;
	}
}
