package ac.soton.codegen.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ac.soton.codegen.xtext.services.SEBGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalSEBParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'schedule'", "'refines'", "'machine'", "';'", "'('", "')*'", "'['", "'[]'", "']'", "'if'", "')'", "'{'", "'}'", "'elseif'", "'else'", "'while'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSEBParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSEBParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSEBParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSEB.g"; }



     	private SEBGrammarAccess grammarAccess;

        public InternalSEBParser(TokenStream input, SEBGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Schedule";
       	}

       	@Override
       	protected SEBGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSchedule"
    // InternalSEB.g:64:1: entryRuleSchedule returns [EObject current=null] : iv_ruleSchedule= ruleSchedule EOF ;
    public final EObject entryRuleSchedule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSchedule = null;


        try {
            // InternalSEB.g:64:49: (iv_ruleSchedule= ruleSchedule EOF )
            // InternalSEB.g:65:2: iv_ruleSchedule= ruleSchedule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScheduleRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSchedule=ruleSchedule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSchedule; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSchedule"


    // $ANTLR start "ruleSchedule"
    // InternalSEB.g:71:1: ruleSchedule returns [EObject current=null] : (otherlv_0= 'schedule' ( (lv_shedName_1_0= RULE_ID ) ) (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )? otherlv_4= 'machine' ( (lv_machineName_5_0= RULE_ID ) ) ( (lv_sheds_6_0= ruleScheduleBody ) )* ) ;
    public final EObject ruleSchedule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_shedName_1_0=null;
        Token otherlv_2=null;
        Token lv_refinedShed_3_0=null;
        Token otherlv_4=null;
        Token lv_machineName_5_0=null;
        EObject lv_sheds_6_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:77:2: ( (otherlv_0= 'schedule' ( (lv_shedName_1_0= RULE_ID ) ) (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )? otherlv_4= 'machine' ( (lv_machineName_5_0= RULE_ID ) ) ( (lv_sheds_6_0= ruleScheduleBody ) )* ) )
            // InternalSEB.g:78:2: (otherlv_0= 'schedule' ( (lv_shedName_1_0= RULE_ID ) ) (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )? otherlv_4= 'machine' ( (lv_machineName_5_0= RULE_ID ) ) ( (lv_sheds_6_0= ruleScheduleBody ) )* )
            {
            // InternalSEB.g:78:2: (otherlv_0= 'schedule' ( (lv_shedName_1_0= RULE_ID ) ) (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )? otherlv_4= 'machine' ( (lv_machineName_5_0= RULE_ID ) ) ( (lv_sheds_6_0= ruleScheduleBody ) )* )
            // InternalSEB.g:79:3: otherlv_0= 'schedule' ( (lv_shedName_1_0= RULE_ID ) ) (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )? otherlv_4= 'machine' ( (lv_machineName_5_0= RULE_ID ) ) ( (lv_sheds_6_0= ruleScheduleBody ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getScheduleAccess().getScheduleKeyword_0());
              		
            }
            // InternalSEB.g:83:3: ( (lv_shedName_1_0= RULE_ID ) )
            // InternalSEB.g:84:4: (lv_shedName_1_0= RULE_ID )
            {
            // InternalSEB.g:84:4: (lv_shedName_1_0= RULE_ID )
            // InternalSEB.g:85:5: lv_shedName_1_0= RULE_ID
            {
            lv_shedName_1_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_shedName_1_0, grammarAccess.getScheduleAccess().getShedNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getScheduleRule());
              					}
              					setWithLastConsumed(
              						current,
              						"shedName",
              						lv_shedName_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalSEB.g:101:3: (otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==12) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalSEB.g:102:4: otherlv_2= 'refines' ( (lv_refinedShed_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getScheduleAccess().getRefinesKeyword_2_0());
                      			
                    }
                    // InternalSEB.g:106:4: ( (lv_refinedShed_3_0= RULE_ID ) )
                    // InternalSEB.g:107:5: (lv_refinedShed_3_0= RULE_ID )
                    {
                    // InternalSEB.g:107:5: (lv_refinedShed_3_0= RULE_ID )
                    // InternalSEB.g:108:6: lv_refinedShed_3_0= RULE_ID
                    {
                    lv_refinedShed_3_0=(Token)match(input,RULE_ID,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_refinedShed_3_0, grammarAccess.getScheduleAccess().getRefinedShedIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getScheduleRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"refinedShed",
                      							lv_refinedShed_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,13,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getScheduleAccess().getMachineKeyword_3());
              		
            }
            // InternalSEB.g:129:3: ( (lv_machineName_5_0= RULE_ID ) )
            // InternalSEB.g:130:4: (lv_machineName_5_0= RULE_ID )
            {
            // InternalSEB.g:130:4: (lv_machineName_5_0= RULE_ID )
            // InternalSEB.g:131:5: lv_machineName_5_0= RULE_ID
            {
            lv_machineName_5_0=(Token)match(input,RULE_ID,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_machineName_5_0, grammarAccess.getScheduleAccess().getMachineNameIDTerminalRuleCall_4_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getScheduleRule());
              					}
              					setWithLastConsumed(
              						current,
              						"machineName",
              						lv_machineName_5_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalSEB.g:147:3: ( (lv_sheds_6_0= ruleScheduleBody ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==15||LA2_0==17||LA2_0==20||LA2_0==26) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSEB.g:148:4: (lv_sheds_6_0= ruleScheduleBody )
            	    {
            	    // InternalSEB.g:148:4: (lv_sheds_6_0= ruleScheduleBody )
            	    // InternalSEB.g:149:5: lv_sheds_6_0= ruleScheduleBody
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getScheduleAccess().getShedsScheduleBodyParserRuleCall_5_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_6);
            	    lv_sheds_6_0=ruleScheduleBody();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getScheduleRule());
            	      					}
            	      					add(
            	      						current,
            	      						"sheds",
            	      						lv_sheds_6_0,
            	      						"ac.soton.codegen.xtext.SEB.ScheduleBody");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSchedule"


    // $ANTLR start "entryRuleScheduleBody"
    // InternalSEB.g:170:1: entryRuleScheduleBody returns [EObject current=null] : iv_ruleScheduleBody= ruleScheduleBody EOF ;
    public final EObject entryRuleScheduleBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScheduleBody = null;


        try {
            // InternalSEB.g:170:53: (iv_ruleScheduleBody= ruleScheduleBody EOF )
            // InternalSEB.g:171:2: iv_ruleScheduleBody= ruleScheduleBody EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScheduleBodyRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleScheduleBody=ruleScheduleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScheduleBody; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScheduleBody"


    // $ANTLR start "ruleScheduleBody"
    // InternalSEB.g:177:1: ruleScheduleBody returns [EObject current=null] : ( ( (lv_sleft_0_0= ruleExpression ) ) (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )? ) ;
    public final EObject ruleScheduleBody() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_sleft_0_0 = null;

        EObject lv_sright_2_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:183:2: ( ( ( (lv_sleft_0_0= ruleExpression ) ) (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )? ) )
            // InternalSEB.g:184:2: ( ( (lv_sleft_0_0= ruleExpression ) ) (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )? )
            {
            // InternalSEB.g:184:2: ( ( (lv_sleft_0_0= ruleExpression ) ) (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )? )
            // InternalSEB.g:185:3: ( (lv_sleft_0_0= ruleExpression ) ) (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )?
            {
            // InternalSEB.g:185:3: ( (lv_sleft_0_0= ruleExpression ) )
            // InternalSEB.g:186:4: (lv_sleft_0_0= ruleExpression )
            {
            // InternalSEB.g:186:4: (lv_sleft_0_0= ruleExpression )
            // InternalSEB.g:187:5: lv_sleft_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getScheduleBodyAccess().getSleftExpressionParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_7);
            lv_sleft_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getScheduleBodyRule());
              					}
              					set(
              						current,
              						"sleft",
              						lv_sleft_0_0,
              						"ac.soton.codegen.xtext.SEB.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSEB.g:204:3: (otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalSEB.g:205:4: otherlv_1= ';' ( (lv_sright_2_0= ruleScheduleBody ) )
                    {
                    otherlv_1=(Token)match(input,14,FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getScheduleBodyAccess().getSemicolonKeyword_1_0());
                      			
                    }
                    // InternalSEB.g:209:4: ( (lv_sright_2_0= ruleScheduleBody ) )
                    // InternalSEB.g:210:5: (lv_sright_2_0= ruleScheduleBody )
                    {
                    // InternalSEB.g:210:5: (lv_sright_2_0= ruleScheduleBody )
                    // InternalSEB.g:211:6: lv_sright_2_0= ruleScheduleBody
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getScheduleBodyAccess().getSrightScheduleBodyParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_sright_2_0=ruleScheduleBody();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getScheduleBodyRule());
                      						}
                      						set(
                      							current,
                      							"sright",
                      							lv_sright_2_0,
                      							"ac.soton.codegen.xtext.SEB.ScheduleBody");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScheduleBody"


    // $ANTLR start "entryRuleExpression"
    // InternalSEB.g:233:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalSEB.g:233:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalSEB.g:234:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalSEB.g:240:1: ruleExpression returns [EObject current=null] : (this_Event_0= ruleEvent | this_Choice_1= ruleChoice | this_Iteration_2= ruleIteration | this_Loop_3= ruleLoop | this_Conditional_4= ruleConditional ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Event_0 = null;

        EObject this_Choice_1 = null;

        EObject this_Iteration_2 = null;

        EObject this_Loop_3 = null;

        EObject this_Conditional_4 = null;



        	enterRule();

        try {
            // InternalSEB.g:246:2: ( (this_Event_0= ruleEvent | this_Choice_1= ruleChoice | this_Iteration_2= ruleIteration | this_Loop_3= ruleLoop | this_Conditional_4= ruleConditional ) )
            // InternalSEB.g:247:2: (this_Event_0= ruleEvent | this_Choice_1= ruleChoice | this_Iteration_2= ruleIteration | this_Loop_3= ruleLoop | this_Conditional_4= ruleConditional )
            {
            // InternalSEB.g:247:2: (this_Event_0= ruleEvent | this_Choice_1= ruleChoice | this_Iteration_2= ruleIteration | this_Loop_3= ruleLoop | this_Conditional_4= ruleConditional )
            int alt4=5;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt4=1;
                }
                break;
            case 17:
                {
                alt4=2;
                }
                break;
            case 15:
                {
                alt4=3;
                }
                break;
            case 26:
                {
                alt4=4;
                }
                break;
            case 20:
                {
                alt4=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalSEB.g:248:3: this_Event_0= ruleEvent
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getEventParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Event_0=ruleEvent();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Event_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalSEB.g:257:3: this_Choice_1= ruleChoice
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getChoiceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Choice_1=ruleChoice();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Choice_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalSEB.g:266:3: this_Iteration_2= ruleIteration
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getIterationParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Iteration_2=ruleIteration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Iteration_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalSEB.g:275:3: this_Loop_3= ruleLoop
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getLoopParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Loop_3=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Loop_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalSEB.g:284:3: this_Conditional_4= ruleConditional
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpressionAccess().getConditionalParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Conditional_4=ruleConditional();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Conditional_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleEvent"
    // InternalSEB.g:296:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalSEB.g:296:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalSEB.g:297:2: iv_ruleEvent= ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvent; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalSEB.g:303:1: ruleEvent returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSEB.g:309:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSEB.g:310:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSEB.g:310:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSEB.g:311:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSEB.g:311:3: (lv_name_0_0= RULE_ID )
            // InternalSEB.g:312:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_name_0_0, grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getEventRule());
              				}
              				setWithLastConsumed(
              					current,
              					"name",
              					lv_name_0_0,
              					"org.eclipse.xtext.common.Terminals.ID");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleIteration"
    // InternalSEB.g:331:1: entryRuleIteration returns [EObject current=null] : iv_ruleIteration= ruleIteration EOF ;
    public final EObject entryRuleIteration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIteration = null;


        try {
            // InternalSEB.g:331:50: (iv_ruleIteration= ruleIteration EOF )
            // InternalSEB.g:332:2: iv_ruleIteration= ruleIteration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIterationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIteration=ruleIteration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIteration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIteration"


    // $ANTLR start "ruleIteration"
    // InternalSEB.g:338:1: ruleIteration returns [EObject current=null] : (otherlv_0= '(' ( (lv_iterationBody_1_0= ruleScheduleBody ) ) otherlv_2= ')*' ) ;
    public final EObject ruleIteration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_iterationBody_1_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:344:2: ( (otherlv_0= '(' ( (lv_iterationBody_1_0= ruleScheduleBody ) ) otherlv_2= ')*' ) )
            // InternalSEB.g:345:2: (otherlv_0= '(' ( (lv_iterationBody_1_0= ruleScheduleBody ) ) otherlv_2= ')*' )
            {
            // InternalSEB.g:345:2: (otherlv_0= '(' ( (lv_iterationBody_1_0= ruleScheduleBody ) ) otherlv_2= ')*' )
            // InternalSEB.g:346:3: otherlv_0= '(' ( (lv_iterationBody_1_0= ruleScheduleBody ) ) otherlv_2= ')*'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIterationAccess().getLeftParenthesisKeyword_0());
              		
            }
            // InternalSEB.g:350:3: ( (lv_iterationBody_1_0= ruleScheduleBody ) )
            // InternalSEB.g:351:4: (lv_iterationBody_1_0= ruleScheduleBody )
            {
            // InternalSEB.g:351:4: (lv_iterationBody_1_0= ruleScheduleBody )
            // InternalSEB.g:352:5: lv_iterationBody_1_0= ruleScheduleBody
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIterationAccess().getIterationBodyScheduleBodyParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_9);
            lv_iterationBody_1_0=ruleScheduleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIterationRule());
              					}
              					set(
              						current,
              						"iterationBody",
              						lv_iterationBody_1_0,
              						"ac.soton.codegen.xtext.SEB.ScheduleBody");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getIterationAccess().getRightParenthesisAsteriskKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIteration"


    // $ANTLR start "entryRuleChoice"
    // InternalSEB.g:377:1: entryRuleChoice returns [EObject current=null] : iv_ruleChoice= ruleChoice EOF ;
    public final EObject entryRuleChoice() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChoice = null;


        try {
            // InternalSEB.g:377:47: (iv_ruleChoice= ruleChoice EOF )
            // InternalSEB.g:378:2: iv_ruleChoice= ruleChoice EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChoiceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleChoice=ruleChoice();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChoice; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChoice"


    // $ANTLR start "ruleChoice"
    // InternalSEB.g:384:1: ruleChoice returns [EObject current=null] : (otherlv_0= '[' ( (lv_lefths_1_0= ruleExpression ) ) (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleChoice() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_lefths_1_0 = null;

        EObject lv_righths_3_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:390:2: ( (otherlv_0= '[' ( (lv_lefths_1_0= ruleExpression ) ) (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )* otherlv_4= ']' ) )
            // InternalSEB.g:391:2: (otherlv_0= '[' ( (lv_lefths_1_0= ruleExpression ) ) (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )* otherlv_4= ']' )
            {
            // InternalSEB.g:391:2: (otherlv_0= '[' ( (lv_lefths_1_0= ruleExpression ) ) (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )* otherlv_4= ']' )
            // InternalSEB.g:392:3: otherlv_0= '[' ( (lv_lefths_1_0= ruleExpression ) ) (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getChoiceAccess().getLeftSquareBracketKeyword_0());
              		
            }
            // InternalSEB.g:396:3: ( (lv_lefths_1_0= ruleExpression ) )
            // InternalSEB.g:397:4: (lv_lefths_1_0= ruleExpression )
            {
            // InternalSEB.g:397:4: (lv_lefths_1_0= ruleExpression )
            // InternalSEB.g:398:5: lv_lefths_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getChoiceAccess().getLefthsExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_lefths_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getChoiceRule());
              					}
              					set(
              						current,
              						"lefths",
              						lv_lefths_1_0,
              						"ac.soton.codegen.xtext.SEB.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalSEB.g:415:3: (otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSEB.g:416:4: otherlv_2= '[]' ( (lv_righths_3_0= ruleExpression ) )
            	    {
            	    otherlv_2=(Token)match(input,18,FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getChoiceAccess().getLeftSquareBracketRightSquareBracketKeyword_2_0());
            	      			
            	    }
            	    // InternalSEB.g:420:4: ( (lv_righths_3_0= ruleExpression ) )
            	    // InternalSEB.g:421:5: (lv_righths_3_0= ruleExpression )
            	    {
            	    // InternalSEB.g:421:5: (lv_righths_3_0= ruleExpression )
            	    // InternalSEB.g:422:6: lv_righths_3_0= ruleExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getChoiceAccess().getRighthsExpressionParserRuleCall_2_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_10);
            	    lv_righths_3_0=ruleExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getChoiceRule());
            	      						}
            	      						add(
            	      							current,
            	      							"righths",
            	      							lv_righths_3_0,
            	      							"ac.soton.codegen.xtext.SEB.Expression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_4=(Token)match(input,19,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getChoiceAccess().getRightSquareBracketKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChoice"


    // $ANTLR start "entryRuleConditional"
    // InternalSEB.g:448:1: entryRuleConditional returns [EObject current=null] : iv_ruleConditional= ruleConditional EOF ;
    public final EObject entryRuleConditional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditional = null;


        try {
            // InternalSEB.g:448:52: (iv_ruleConditional= ruleConditional EOF )
            // InternalSEB.g:449:2: iv_ruleConditional= ruleConditional EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConditional=ruleConditional();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditional; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditional"


    // $ANTLR start "ruleConditional"
    // InternalSEB.g:455:1: ruleConditional returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleScheduleBody ) ) otherlv_6= '}' (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )* ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )? ) ;
    public final EObject ruleConditional() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        EObject lv_condition_2_0 = null;

        EObject lv_then_5_0 = null;

        EObject lv_elseCond_9_0 = null;

        EObject lv_elseif_12_0 = null;

        EObject lv_else_16_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:461:2: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleScheduleBody ) ) otherlv_6= '}' (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )* ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )? ) )
            // InternalSEB.g:462:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleScheduleBody ) ) otherlv_6= '}' (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )* ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )? )
            {
            // InternalSEB.g:462:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleScheduleBody ) ) otherlv_6= '}' (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )* ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )? )
            // InternalSEB.g:463:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleScheduleBody ) ) otherlv_6= '}' (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )* ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )?
            {
            otherlv_0=(Token)match(input,20,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getConditionalAccess().getIfKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,15,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getConditionalAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalSEB.g:471:3: ( (lv_condition_2_0= ruleBooleanExpression ) )
            // InternalSEB.g:472:4: (lv_condition_2_0= ruleBooleanExpression )
            {
            // InternalSEB.g:472:4: (lv_condition_2_0= ruleBooleanExpression )
            // InternalSEB.g:473:5: lv_condition_2_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalAccess().getConditionBooleanExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_13);
            lv_condition_2_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_2_0,
              						"ac.soton.codegen.xtext.SEB.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getConditionalAccess().getRightParenthesisKeyword_3());
              		
            }
            otherlv_4=(Token)match(input,22,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getConditionalAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalSEB.g:498:3: ( (lv_then_5_0= ruleScheduleBody ) )
            // InternalSEB.g:499:4: (lv_then_5_0= ruleScheduleBody )
            {
            // InternalSEB.g:499:4: (lv_then_5_0= ruleScheduleBody )
            // InternalSEB.g:500:5: lv_then_5_0= ruleScheduleBody
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalAccess().getThenScheduleBodyParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_then_5_0=ruleScheduleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalRule());
              					}
              					set(
              						current,
              						"then",
              						lv_then_5_0,
              						"ac.soton.codegen.xtext.SEB.ScheduleBody");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getConditionalAccess().getRightCurlyBracketKeyword_6());
              		
            }
            // InternalSEB.g:521:3: (otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==24) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalSEB.g:522:4: otherlv_7= 'elseif' otherlv_8= '(' ( (lv_elseCond_9_0= ruleBooleanExpression ) ) otherlv_10= ')' otherlv_11= '{' ( (lv_elseif_12_0= ruleScheduleBody ) ) otherlv_13= '}'
            	    {
            	    otherlv_7=(Token)match(input,24,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_7, grammarAccess.getConditionalAccess().getElseifKeyword_7_0());
            	      			
            	    }
            	    otherlv_8=(Token)match(input,15,FOLLOW_12); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_8, grammarAccess.getConditionalAccess().getLeftParenthesisKeyword_7_1());
            	      			
            	    }
            	    // InternalSEB.g:530:4: ( (lv_elseCond_9_0= ruleBooleanExpression ) )
            	    // InternalSEB.g:531:5: (lv_elseCond_9_0= ruleBooleanExpression )
            	    {
            	    // InternalSEB.g:531:5: (lv_elseCond_9_0= ruleBooleanExpression )
            	    // InternalSEB.g:532:6: lv_elseCond_9_0= ruleBooleanExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getConditionalAccess().getElseCondBooleanExpressionParserRuleCall_7_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_13);
            	    lv_elseCond_9_0=ruleBooleanExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getConditionalRule());
            	      						}
            	      						add(
            	      							current,
            	      							"elseCond",
            	      							lv_elseCond_9_0,
            	      							"ac.soton.codegen.xtext.SEB.BooleanExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    otherlv_10=(Token)match(input,21,FOLLOW_14); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_10, grammarAccess.getConditionalAccess().getRightParenthesisKeyword_7_3());
            	      			
            	    }
            	    otherlv_11=(Token)match(input,22,FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_11, grammarAccess.getConditionalAccess().getLeftCurlyBracketKeyword_7_4());
            	      			
            	    }
            	    // InternalSEB.g:557:4: ( (lv_elseif_12_0= ruleScheduleBody ) )
            	    // InternalSEB.g:558:5: (lv_elseif_12_0= ruleScheduleBody )
            	    {
            	    // InternalSEB.g:558:5: (lv_elseif_12_0= ruleScheduleBody )
            	    // InternalSEB.g:559:6: lv_elseif_12_0= ruleScheduleBody
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getConditionalAccess().getElseifScheduleBodyParserRuleCall_7_5_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_elseif_12_0=ruleScheduleBody();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getConditionalRule());
            	      						}
            	      						add(
            	      							current,
            	      							"elseif",
            	      							lv_elseif_12_0,
            	      							"ac.soton.codegen.xtext.SEB.ScheduleBody");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    otherlv_13=(Token)match(input,23,FOLLOW_16); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_13, grammarAccess.getConditionalAccess().getRightCurlyBracketKeyword_7_6());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalSEB.g:581:3: ( ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==25) && (synpred1_InternalSEB())) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalSEB.g:582:4: ( ( 'else' )=>otherlv_14= 'else' ) otherlv_15= '{' ( (lv_else_16_0= ruleScheduleBody ) ) otherlv_17= '}'
                    {
                    // InternalSEB.g:582:4: ( ( 'else' )=>otherlv_14= 'else' )
                    // InternalSEB.g:583:5: ( 'else' )=>otherlv_14= 'else'
                    {
                    otherlv_14=(Token)match(input,25,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_14, grammarAccess.getConditionalAccess().getElseKeyword_8_0());
                      				
                    }

                    }

                    otherlv_15=(Token)match(input,22,FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_15, grammarAccess.getConditionalAccess().getLeftCurlyBracketKeyword_8_1());
                      			
                    }
                    // InternalSEB.g:593:4: ( (lv_else_16_0= ruleScheduleBody ) )
                    // InternalSEB.g:594:5: (lv_else_16_0= ruleScheduleBody )
                    {
                    // InternalSEB.g:594:5: (lv_else_16_0= ruleScheduleBody )
                    // InternalSEB.g:595:6: lv_else_16_0= ruleScheduleBody
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConditionalAccess().getElseScheduleBodyParserRuleCall_8_2_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_else_16_0=ruleScheduleBody();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConditionalRule());
                      						}
                      						set(
                      							current,
                      							"else",
                      							lv_else_16_0,
                      							"ac.soton.codegen.xtext.SEB.ScheduleBody");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_17=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_17, grammarAccess.getConditionalAccess().getRightCurlyBracketKeyword_8_3());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditional"


    // $ANTLR start "entryRuleLoop"
    // InternalSEB.g:621:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // InternalSEB.g:621:45: (iv_ruleLoop= ruleLoop EOF )
            // InternalSEB.g:622:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalSEB.g:628:1: ruleLoop returns [EObject current=null] : (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_body_5_0= ruleScheduleBody ) ) otherlv_6= '}' ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_condition_2_0 = null;

        EObject lv_body_5_0 = null;



        	enterRule();

        try {
            // InternalSEB.g:634:2: ( (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_body_5_0= ruleScheduleBody ) ) otherlv_6= '}' ) )
            // InternalSEB.g:635:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_body_5_0= ruleScheduleBody ) ) otherlv_6= '}' )
            {
            // InternalSEB.g:635:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_body_5_0= ruleScheduleBody ) ) otherlv_6= '}' )
            // InternalSEB.g:636:3: otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleBooleanExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_body_5_0= ruleScheduleBody ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,15,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getLoopAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalSEB.g:644:3: ( (lv_condition_2_0= ruleBooleanExpression ) )
            // InternalSEB.g:645:4: (lv_condition_2_0= ruleBooleanExpression )
            {
            // InternalSEB.g:645:4: (lv_condition_2_0= ruleBooleanExpression )
            // InternalSEB.g:646:5: lv_condition_2_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getLoopAccess().getConditionBooleanExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_13);
            lv_condition_2_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getLoopRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_2_0,
              						"ac.soton.codegen.xtext.SEB.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getLoopAccess().getRightParenthesisKeyword_3());
              		
            }
            otherlv_4=(Token)match(input,22,FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getLoopAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalSEB.g:671:3: ( (lv_body_5_0= ruleScheduleBody ) )
            // InternalSEB.g:672:4: (lv_body_5_0= ruleScheduleBody )
            {
            // InternalSEB.g:672:4: (lv_body_5_0= ruleScheduleBody )
            // InternalSEB.g:673:5: lv_body_5_0= ruleScheduleBody
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getLoopAccess().getBodyScheduleBodyParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_body_5_0=ruleScheduleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getLoopRule());
              					}
              					set(
              						current,
              						"body",
              						lv_body_5_0,
              						"ac.soton.codegen.xtext.SEB.ScheduleBody");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getLoopAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleBooleanExpression"
    // InternalSEB.g:698:1: entryRuleBooleanExpression returns [EObject current=null] : iv_ruleBooleanExpression= ruleBooleanExpression EOF ;
    public final EObject entryRuleBooleanExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanExpression = null;


        try {
            // InternalSEB.g:698:58: (iv_ruleBooleanExpression= ruleBooleanExpression EOF )
            // InternalSEB.g:699:2: iv_ruleBooleanExpression= ruleBooleanExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBooleanExpression=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanExpression"


    // $ANTLR start "ruleBooleanExpression"
    // InternalSEB.g:705:1: ruleBooleanExpression returns [EObject current=null] : ( (lv_predicate_0_0= RULE_STRING ) ) ;
    public final EObject ruleBooleanExpression() throws RecognitionException {
        EObject current = null;

        Token lv_predicate_0_0=null;


        	enterRule();

        try {
            // InternalSEB.g:711:2: ( ( (lv_predicate_0_0= RULE_STRING ) ) )
            // InternalSEB.g:712:2: ( (lv_predicate_0_0= RULE_STRING ) )
            {
            // InternalSEB.g:712:2: ( (lv_predicate_0_0= RULE_STRING ) )
            // InternalSEB.g:713:3: (lv_predicate_0_0= RULE_STRING )
            {
            // InternalSEB.g:713:3: (lv_predicate_0_0= RULE_STRING )
            // InternalSEB.g:714:4: lv_predicate_0_0= RULE_STRING
            {
            lv_predicate_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_predicate_0_0, grammarAccess.getBooleanExpressionAccess().getPredicateSTRINGTerminalRuleCall_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getBooleanExpressionRule());
              				}
              				setWithLastConsumed(
              					current,
              					"predicate",
              					lv_predicate_0_0,
              					"org.eclipse.xtext.common.Terminals.STRING");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanExpression"

    // $ANTLR start synpred1_InternalSEB
    public final void synpred1_InternalSEB_fragment() throws RecognitionException {   
        // InternalSEB.g:583:5: ( 'else' )
        // InternalSEB.g:583:6: 'else'
        {
        match(input,25,FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalSEB

    // Delegated rules

    public final boolean synpred1_InternalSEB() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalSEB_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000004128012L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000004128010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000003000002L});

}