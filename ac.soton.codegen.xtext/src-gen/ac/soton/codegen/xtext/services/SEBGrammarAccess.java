/*
 * generated by Xtext 2.13.0
 */
package ac.soton.codegen.xtext.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractGrammarElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class SEBGrammarAccess extends AbstractGrammarElementFinder {
	
	public class ScheduleElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Schedule");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cScheduleKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cShedNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cShedNameIDTerminalRuleCall_1_0 = (RuleCall)cShedNameAssignment_1.eContents().get(0);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cRefinesKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cRefinedShedAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cRefinedShedIDTerminalRuleCall_2_1_0 = (RuleCall)cRefinedShedAssignment_2_1.eContents().get(0);
		private final Keyword cMachineKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cMachineNameAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cMachineNameIDTerminalRuleCall_4_0 = (RuleCall)cMachineNameAssignment_4.eContents().get(0);
		private final Assignment cShedsAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cShedsScheduleBodyParserRuleCall_5_0 = (RuleCall)cShedsAssignment_5.eContents().get(0);
		
		///*Model:
		//	Schedule
		//;
		//*/ Schedule:
		//	'schedule' shedName=ID ('refines' refinedShed=ID)?
		//	'machine' machineName=ID
		//	sheds+=ScheduleBody*;
		@Override public ParserRule getRule() { return rule; }
		
		//'schedule' shedName=ID ('refines' refinedShed=ID)? 'machine' machineName=ID sheds+=ScheduleBody*
		public Group getGroup() { return cGroup; }
		
		//'schedule'
		public Keyword getScheduleKeyword_0() { return cScheduleKeyword_0; }
		
		//shedName=ID
		public Assignment getShedNameAssignment_1() { return cShedNameAssignment_1; }
		
		//ID
		public RuleCall getShedNameIDTerminalRuleCall_1_0() { return cShedNameIDTerminalRuleCall_1_0; }
		
		//('refines' refinedShed=ID)?
		public Group getGroup_2() { return cGroup_2; }
		
		//'refines'
		public Keyword getRefinesKeyword_2_0() { return cRefinesKeyword_2_0; }
		
		//refinedShed=ID
		public Assignment getRefinedShedAssignment_2_1() { return cRefinedShedAssignment_2_1; }
		
		//ID
		public RuleCall getRefinedShedIDTerminalRuleCall_2_1_0() { return cRefinedShedIDTerminalRuleCall_2_1_0; }
		
		//'machine'
		public Keyword getMachineKeyword_3() { return cMachineKeyword_3; }
		
		//machineName=ID
		public Assignment getMachineNameAssignment_4() { return cMachineNameAssignment_4; }
		
		//ID
		public RuleCall getMachineNameIDTerminalRuleCall_4_0() { return cMachineNameIDTerminalRuleCall_4_0; }
		
		//sheds+=ScheduleBody*
		public Assignment getShedsAssignment_5() { return cShedsAssignment_5; }
		
		//ScheduleBody
		public RuleCall getShedsScheduleBodyParserRuleCall_5_0() { return cShedsScheduleBodyParserRuleCall_5_0; }
	}
	public class ScheduleBodyElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.ScheduleBody");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cSleftAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cSleftExpressionParserRuleCall_0_0 = (RuleCall)cSleftAssignment_0.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cSemicolonKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Assignment cSrightAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final RuleCall cSrightScheduleBodyParserRuleCall_1_1_0 = (RuleCall)cSrightAssignment_1_1.eContents().get(0);
		
		//ScheduleBody:
		//	sleft=Expression (';' sright=ScheduleBody)?;
		@Override public ParserRule getRule() { return rule; }
		
		//sleft=Expression (';' sright=ScheduleBody)?
		public Group getGroup() { return cGroup; }
		
		//sleft=Expression
		public Assignment getSleftAssignment_0() { return cSleftAssignment_0; }
		
		//Expression
		public RuleCall getSleftExpressionParserRuleCall_0_0() { return cSleftExpressionParserRuleCall_0_0; }
		
		//(';' sright=ScheduleBody)?
		public Group getGroup_1() { return cGroup_1; }
		
		//';'
		public Keyword getSemicolonKeyword_1_0() { return cSemicolonKeyword_1_0; }
		
		//sright=ScheduleBody
		public Assignment getSrightAssignment_1_1() { return cSrightAssignment_1_1; }
		
		//ScheduleBody
		public RuleCall getSrightScheduleBodyParserRuleCall_1_1_0() { return cSrightScheduleBodyParserRuleCall_1_1_0; }
	}
	public class ExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Expression");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cEventParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cChoiceParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cIterationParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cLoopParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		private final RuleCall cConditionalParserRuleCall_4 = (RuleCall)cAlternatives.eContents().get(4);
		
		//Expression:
		//	Event | Choice | Iteration | Loop | Conditional;
		@Override public ParserRule getRule() { return rule; }
		
		//Event | Choice | Iteration | Loop | Conditional
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//Event
		public RuleCall getEventParserRuleCall_0() { return cEventParserRuleCall_0; }
		
		//Choice
		public RuleCall getChoiceParserRuleCall_1() { return cChoiceParserRuleCall_1; }
		
		//Iteration
		public RuleCall getIterationParserRuleCall_2() { return cIterationParserRuleCall_2; }
		
		//Loop
		public RuleCall getLoopParserRuleCall_3() { return cLoopParserRuleCall_3; }
		
		//Conditional
		public RuleCall getConditionalParserRuleCall_4() { return cConditionalParserRuleCall_4; }
	}
	public class EventElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Event");
		private final Assignment cNameAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_0 = (RuleCall)cNameAssignment.eContents().get(0);
		
		//Event:
		//	name=ID;
		@Override public ParserRule getRule() { return rule; }
		
		//name=ID
		public Assignment getNameAssignment() { return cNameAssignment; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_0() { return cNameIDTerminalRuleCall_0; }
	}
	public class IterationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Iteration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftParenthesisKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cIterationBodyAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cIterationBodyScheduleBodyParserRuleCall_1_0 = (RuleCall)cIterationBodyAssignment_1.eContents().get(0);
		private final Keyword cRightParenthesisAsteriskKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//Iteration:
		//	'(' iterationBody=ScheduleBody ')*';
		@Override public ParserRule getRule() { return rule; }
		
		//'(' iterationBody=ScheduleBody ')*'
		public Group getGroup() { return cGroup; }
		
		//'('
		public Keyword getLeftParenthesisKeyword_0() { return cLeftParenthesisKeyword_0; }
		
		//iterationBody=ScheduleBody
		public Assignment getIterationBodyAssignment_1() { return cIterationBodyAssignment_1; }
		
		//ScheduleBody
		public RuleCall getIterationBodyScheduleBodyParserRuleCall_1_0() { return cIterationBodyScheduleBodyParserRuleCall_1_0; }
		
		//')*'
		public Keyword getRightParenthesisAsteriskKeyword_2() { return cRightParenthesisAsteriskKeyword_2; }
	}
	public class ChoiceElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Choice");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cLefthsAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cLefthsExpressionParserRuleCall_1_0 = (RuleCall)cLefthsAssignment_1.eContents().get(0);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cLeftSquareBracketRightSquareBracketKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cRighthsAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cRighthsExpressionParserRuleCall_2_1_0 = (RuleCall)cRighthsAssignment_2_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//Choice:
		//	'[' lefths=Expression ('[]' righths+=Expression)* ']';
		@Override public ParserRule getRule() { return rule; }
		
		//'[' lefths=Expression ('[]' righths+=Expression)* ']'
		public Group getGroup() { return cGroup; }
		
		//'['
		public Keyword getLeftSquareBracketKeyword_0() { return cLeftSquareBracketKeyword_0; }
		
		//lefths=Expression
		public Assignment getLefthsAssignment_1() { return cLefthsAssignment_1; }
		
		//Expression
		public RuleCall getLefthsExpressionParserRuleCall_1_0() { return cLefthsExpressionParserRuleCall_1_0; }
		
		//('[]' righths+=Expression)*
		public Group getGroup_2() { return cGroup_2; }
		
		//'[]'
		public Keyword getLeftSquareBracketRightSquareBracketKeyword_2_0() { return cLeftSquareBracketRightSquareBracketKeyword_2_0; }
		
		//righths+=Expression
		public Assignment getRighthsAssignment_2_1() { return cRighthsAssignment_2_1; }
		
		//Expression
		public RuleCall getRighthsExpressionParserRuleCall_2_1_0() { return cRighthsExpressionParserRuleCall_2_1_0; }
		
		//']'
		public Keyword getRightSquareBracketKeyword_3() { return cRightSquareBracketKeyword_3; }
	}
	public class ConditionalElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Conditional");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cIfKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cConditionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cConditionBooleanExpressionParserRuleCall_2_0 = (RuleCall)cConditionAssignment_2.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Keyword cLeftCurlyBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cThenAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cThenScheduleBodyParserRuleCall_5_0 = (RuleCall)cThenAssignment_5.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_6 = (Keyword)cGroup.eContents().get(6);
		private final Group cGroup_7 = (Group)cGroup.eContents().get(7);
		private final Keyword cElseifKeyword_7_0 = (Keyword)cGroup_7.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_7_1 = (Keyword)cGroup_7.eContents().get(1);
		private final Assignment cElseCondAssignment_7_2 = (Assignment)cGroup_7.eContents().get(2);
		private final RuleCall cElseCondBooleanExpressionParserRuleCall_7_2_0 = (RuleCall)cElseCondAssignment_7_2.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_7_3 = (Keyword)cGroup_7.eContents().get(3);
		private final Keyword cLeftCurlyBracketKeyword_7_4 = (Keyword)cGroup_7.eContents().get(4);
		private final Assignment cElseifAssignment_7_5 = (Assignment)cGroup_7.eContents().get(5);
		private final RuleCall cElseifScheduleBodyParserRuleCall_7_5_0 = (RuleCall)cElseifAssignment_7_5.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_7_6 = (Keyword)cGroup_7.eContents().get(6);
		private final Group cGroup_8 = (Group)cGroup.eContents().get(8);
		private final Keyword cElseKeyword_8_0 = (Keyword)cGroup_8.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_8_1 = (Keyword)cGroup_8.eContents().get(1);
		private final Assignment cElseAssignment_8_2 = (Assignment)cGroup_8.eContents().get(2);
		private final RuleCall cElseScheduleBodyParserRuleCall_8_2_0 = (RuleCall)cElseAssignment_8_2.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_8_3 = (Keyword)cGroup_8.eContents().get(3);
		
		//Conditional:
		//	'if' '(' condition=BooleanExpression ')' '{'
		//	then=ScheduleBody
		//	'}' ('elseif' '(' elseCond+=BooleanExpression ')' '{'
		//	elseif+=ScheduleBody
		//	'}')* (=> 'else' '{' else=ScheduleBody '}')?;
		@Override public ParserRule getRule() { return rule; }
		
		//'if' '(' condition=BooleanExpression ')' '{' then=ScheduleBody '}' ('elseif' '(' elseCond+=BooleanExpression ')' '{'
		//elseif+=ScheduleBody '}')* (=> 'else' '{' else=ScheduleBody '}')?
		public Group getGroup() { return cGroup; }
		
		//'if'
		public Keyword getIfKeyword_0() { return cIfKeyword_0; }
		
		//'('
		public Keyword getLeftParenthesisKeyword_1() { return cLeftParenthesisKeyword_1; }
		
		//condition=BooleanExpression
		public Assignment getConditionAssignment_2() { return cConditionAssignment_2; }
		
		//BooleanExpression
		public RuleCall getConditionBooleanExpressionParserRuleCall_2_0() { return cConditionBooleanExpressionParserRuleCall_2_0; }
		
		//')'
		public Keyword getRightParenthesisKeyword_3() { return cRightParenthesisKeyword_3; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_4() { return cLeftCurlyBracketKeyword_4; }
		
		//then=ScheduleBody
		public Assignment getThenAssignment_5() { return cThenAssignment_5; }
		
		//ScheduleBody
		public RuleCall getThenScheduleBodyParserRuleCall_5_0() { return cThenScheduleBodyParserRuleCall_5_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_6() { return cRightCurlyBracketKeyword_6; }
		
		//('elseif' '(' elseCond+=BooleanExpression ')' '{' elseif+=ScheduleBody '}')*
		public Group getGroup_7() { return cGroup_7; }
		
		//'elseif'
		public Keyword getElseifKeyword_7_0() { return cElseifKeyword_7_0; }
		
		//'('
		public Keyword getLeftParenthesisKeyword_7_1() { return cLeftParenthesisKeyword_7_1; }
		
		//elseCond+=BooleanExpression
		public Assignment getElseCondAssignment_7_2() { return cElseCondAssignment_7_2; }
		
		//BooleanExpression
		public RuleCall getElseCondBooleanExpressionParserRuleCall_7_2_0() { return cElseCondBooleanExpressionParserRuleCall_7_2_0; }
		
		//')'
		public Keyword getRightParenthesisKeyword_7_3() { return cRightParenthesisKeyword_7_3; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_7_4() { return cLeftCurlyBracketKeyword_7_4; }
		
		//elseif+=ScheduleBody
		public Assignment getElseifAssignment_7_5() { return cElseifAssignment_7_5; }
		
		//ScheduleBody
		public RuleCall getElseifScheduleBodyParserRuleCall_7_5_0() { return cElseifScheduleBodyParserRuleCall_7_5_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_7_6() { return cRightCurlyBracketKeyword_7_6; }
		
		//(=> 'else' '{' else=ScheduleBody '}')?
		public Group getGroup_8() { return cGroup_8; }
		
		//=> 'else'
		public Keyword getElseKeyword_8_0() { return cElseKeyword_8_0; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_8_1() { return cLeftCurlyBracketKeyword_8_1; }
		
		//else=ScheduleBody
		public Assignment getElseAssignment_8_2() { return cElseAssignment_8_2; }
		
		//ScheduleBody
		public RuleCall getElseScheduleBodyParserRuleCall_8_2_0() { return cElseScheduleBodyParserRuleCall_8_2_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_8_3() { return cRightCurlyBracketKeyword_8_3; }
	}
	public class LoopElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.Loop");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cWhileKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cConditionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cConditionBooleanExpressionParserRuleCall_2_0 = (RuleCall)cConditionAssignment_2.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Keyword cLeftCurlyBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cBodyAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cBodyScheduleBodyParserRuleCall_5_0 = (RuleCall)cBodyAssignment_5.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_6 = (Keyword)cGroup.eContents().get(6);
		
		//Loop:
		//	'while' '(' condition=BooleanExpression ')' '{'
		//	body=ScheduleBody
		//	'}';
		@Override public ParserRule getRule() { return rule; }
		
		//'while' '(' condition=BooleanExpression ')' '{' body=ScheduleBody '}'
		public Group getGroup() { return cGroup; }
		
		//'while'
		public Keyword getWhileKeyword_0() { return cWhileKeyword_0; }
		
		//'('
		public Keyword getLeftParenthesisKeyword_1() { return cLeftParenthesisKeyword_1; }
		
		//condition=BooleanExpression
		public Assignment getConditionAssignment_2() { return cConditionAssignment_2; }
		
		//BooleanExpression
		public RuleCall getConditionBooleanExpressionParserRuleCall_2_0() { return cConditionBooleanExpressionParserRuleCall_2_0; }
		
		//')'
		public Keyword getRightParenthesisKeyword_3() { return cRightParenthesisKeyword_3; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_4() { return cLeftCurlyBracketKeyword_4; }
		
		//body=ScheduleBody
		public Assignment getBodyAssignment_5() { return cBodyAssignment_5; }
		
		//ScheduleBody
		public RuleCall getBodyScheduleBodyParserRuleCall_5_0() { return cBodyScheduleBodyParserRuleCall_5_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_6() { return cRightCurlyBracketKeyword_6; }
	}
	public class BooleanExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ac.soton.codegen.xtext.SEB.BooleanExpression");
		private final Assignment cPredicateAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cPredicateSTRINGTerminalRuleCall_0 = (RuleCall)cPredicateAssignment.eContents().get(0);
		
		//BooleanExpression:
		//	predicate=STRING;
		@Override public ParserRule getRule() { return rule; }
		
		//predicate=STRING
		public Assignment getPredicateAssignment() { return cPredicateAssignment; }
		
		//STRING
		public RuleCall getPredicateSTRINGTerminalRuleCall_0() { return cPredicateSTRINGTerminalRuleCall_0; }
	}
	
	
	private final ScheduleElements pSchedule;
	private final ScheduleBodyElements pScheduleBody;
	private final ExpressionElements pExpression;
	private final EventElements pEvent;
	private final IterationElements pIteration;
	private final ChoiceElements pChoice;
	private final ConditionalElements pConditional;
	private final LoopElements pLoop;
	private final BooleanExpressionElements pBooleanExpression;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public SEBGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pSchedule = new ScheduleElements();
		this.pScheduleBody = new ScheduleBodyElements();
		this.pExpression = new ExpressionElements();
		this.pEvent = new EventElements();
		this.pIteration = new IterationElements();
		this.pChoice = new ChoiceElements();
		this.pConditional = new ConditionalElements();
		this.pLoop = new LoopElements();
		this.pBooleanExpression = new BooleanExpressionElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("ac.soton.codegen.xtext.SEB".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	///*Model:
	//	Schedule
	//;
	//*/ Schedule:
	//	'schedule' shedName=ID ('refines' refinedShed=ID)?
	//	'machine' machineName=ID
	//	sheds+=ScheduleBody*;
	public ScheduleElements getScheduleAccess() {
		return pSchedule;
	}
	
	public ParserRule getScheduleRule() {
		return getScheduleAccess().getRule();
	}
	
	//ScheduleBody:
	//	sleft=Expression (';' sright=ScheduleBody)?;
	public ScheduleBodyElements getScheduleBodyAccess() {
		return pScheduleBody;
	}
	
	public ParserRule getScheduleBodyRule() {
		return getScheduleBodyAccess().getRule();
	}
	
	//Expression:
	//	Event | Choice | Iteration | Loop | Conditional;
	public ExpressionElements getExpressionAccess() {
		return pExpression;
	}
	
	public ParserRule getExpressionRule() {
		return getExpressionAccess().getRule();
	}
	
	//Event:
	//	name=ID;
	public EventElements getEventAccess() {
		return pEvent;
	}
	
	public ParserRule getEventRule() {
		return getEventAccess().getRule();
	}
	
	//Iteration:
	//	'(' iterationBody=ScheduleBody ')*';
	public IterationElements getIterationAccess() {
		return pIteration;
	}
	
	public ParserRule getIterationRule() {
		return getIterationAccess().getRule();
	}
	
	//Choice:
	//	'[' lefths=Expression ('[]' righths+=Expression)* ']';
	public ChoiceElements getChoiceAccess() {
		return pChoice;
	}
	
	public ParserRule getChoiceRule() {
		return getChoiceAccess().getRule();
	}
	
	//Conditional:
	//	'if' '(' condition=BooleanExpression ')' '{'
	//	then=ScheduleBody
	//	'}' ('elseif' '(' elseCond+=BooleanExpression ')' '{'
	//	elseif+=ScheduleBody
	//	'}')* (=> 'else' '{' else=ScheduleBody '}')?;
	public ConditionalElements getConditionalAccess() {
		return pConditional;
	}
	
	public ParserRule getConditionalRule() {
		return getConditionalAccess().getRule();
	}
	
	//Loop:
	//	'while' '(' condition=BooleanExpression ')' '{'
	//	body=ScheduleBody
	//	'}';
	public LoopElements getLoopAccess() {
		return pLoop;
	}
	
	public ParserRule getLoopRule() {
		return getLoopAccess().getRule();
	}
	
	//BooleanExpression:
	//	predicate=STRING;
	public BooleanExpressionElements getBooleanExpressionAccess() {
		return pBooleanExpression;
	}
	
	public ParserRule getBooleanExpressionRule() {
		return getBooleanExpressionAccess().getRule();
	}
	
	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' | "'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
