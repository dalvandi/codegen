/**
 * generated by Xtext 2.13.0
 */
package ac.soton.codegen.xtext.sEB;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.soton.codegen.xtext.sEB.SEBPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
