/**
 * generated by Xtext 2.13.0
 */
package ac.soton.codegen.xtext.sEB;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ac.soton.codegen.xtext.sEB.Loop#getCondition <em>Condition</em>}</li>
 *   <li>{@link ac.soton.codegen.xtext.sEB.Loop#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see ac.soton.codegen.xtext.sEB.SEBPackage#getLoop()
 * @model
 * @generated
 */
public interface Loop extends Expression
{
  /**
   * Returns the value of the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' containment reference.
   * @see #setCondition(BooleanExpression)
   * @see ac.soton.codegen.xtext.sEB.SEBPackage#getLoop_Condition()
   * @model containment="true"
   * @generated
   */
  BooleanExpression getCondition();

  /**
   * Sets the value of the '{@link ac.soton.codegen.xtext.sEB.Loop#getCondition <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' containment reference.
   * @see #getCondition()
   * @generated
   */
  void setCondition(BooleanExpression value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(ScheduleBody)
   * @see ac.soton.codegen.xtext.sEB.SEBPackage#getLoop_Body()
   * @model containment="true"
   * @generated
   */
  ScheduleBody getBody();

  /**
   * Sets the value of the '{@link ac.soton.codegen.xtext.sEB.Loop#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(ScheduleBody value);

} // Loop
