package ac.soton.codegen.ui.internal.ui;

import org.eventb.ui.IEventBSharedImages;

public interface IScheduleImages extends IEventBSharedImages{
	public static final String IMG_Given = "Given";
	public static final String IMG_Given_PATH = "Icons/given.gif";
	
	public static final String IMG_INFER = "Infer";
	public static final String IMG_INFER_PATH = "Icons/infer.gif";
	
	public static final String IMG_INFERENCE = "Inference Rule";
	public static final String IMG_INFERENCE_PATH = "Icons/inference.gif";
	
	public static final String IMG_META_VAR = "Meta Variable";
	public static final String IMG_META_VAR_PATH = "Icons/metaVar";
	
	public static final String IMG_DDef = "Direct Definition";
	public static final String IMG_DDef_PATH = "Icons/directDef.gif";
	
	public static final String IMG_RDef = "Recursive Definition";
	public static final String IMG_RDef_PATH = "Icons/recDef";
	
	public static final String IMG_RDef_CASE = "Recursive Definition Case";
	public static final String IMG_RDef_CASE_PATH = "Icons/recCase";
	
	public static final String IMG_DATATYPE_CONS = "Datatype Constructor";
	public static final String IMG_DATATYPE_CONS_PATH = "Icons/cons.gif";
	
	public static final String IMG_DATATYPE_DEST = "Datatype Destructor";
	public static final String IMG_DATATYPE_DEST_PATH = "Icons/dest.gif";
	
	public static final String IMG_DATATYPE = "Datatype";
	public static final String IMG_DATATYPE_PATH = "Icons/datatype.gif";
	
	public static final String IMG_RULES_BLOCK = "Rules Block";
	public static final String IMG_RULES_BLOCK_PATH = "Icons/block.gif";
	
	public static final String IMG_TTHEOREM = "Theory Theorem";
	public static final String IMG_TTHEOREM_PATH = "Icons/theorem.gif";
	
	public static final String IMG_OPERATOR = "Operator";
	public static final String IMG_OPERATOR_PATH = "Icons/op.gif";
	
	public static final String IMG_OPERATOR_WD = "Operator WD";
	public static final String IMG_OPERATOR_WD_PATH = "Icons/opWd.gif";
	
	public static final String IMG_REWRITE_RULE = "Rewrite Rule";
	public static final String IMG_REWRITE_RULE_PATH = "Icons/rew.gif";
	
	public static final String IMG_RULE_RHS = "RHS";
	public static final String IMG_RULE_RHS_PATH = "Icons/rhs.gif";
	
	public static final String IMG_TYPEPAR = "Type Parameter";
	public static final String IMG_TYPEPAR_PATH = "Icons/typePar.gif";
	
	public static final String IMG_THEORY = "Theory";
	public static final String IMG_THEORY_PATH = "Icons/thy.gif";
	
	public static final String IMG_DTHEORY = "Deployed Theory";
	public static final String IMG_DTHEORY_PATH = "Icons/thyDep.gif";
	
	public static final String IMG_OTHEORY = "Outdated Theory";
	public static final String IMG_OTHEORY_PATH = "Icons/thyOut.gif";
}
