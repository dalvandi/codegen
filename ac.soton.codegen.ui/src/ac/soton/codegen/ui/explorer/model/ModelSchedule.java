package ac.soton.codegen.ui.explorer.model;

import java.util.HashMap;

import org.eventb.core.IEventBRoot;
import org.eventb.core.IPORoot;
import org.eventb.core.IPOSequent;
import org.eventb.core.IPOSource;
import org.eventb.core.IPSRoot;
import org.eventb.core.IPSStatus;
import org.eventb.internal.ui.UIUtils;
import ac.soton.codegen.core.IScheduleAuxRoot;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.RodinDBException;

import fr.systerel.internal.explorer.model.IModelElement;
import fr.systerel.internal.explorer.navigator.ExplorerUtils;


@SuppressWarnings("restriction")
public class ModelSchedule extends ScheduleModelPOContainer{
	
	/**
	 * The nodes are used by the ContentProviders to present a node in the tree
	 * above elements such as Axioms or Theorems.
	 */
	public final ScheduleModelElementNode po_node;
	

	
	private IScheduleAuxRoot theoryRoot;

	//indicate whether the poRoot or the psRoot should be processed freshly
	public boolean psNeedsProcessing = true;
	public boolean poNeedsProcessing = true;

	
	/**
	 * Creates a ModelContext from a given IContextRoot
	 * @param root	The ContextRoot that this ModelContext is based on.
	 */
	public ModelSchedule(IScheduleAuxRoot root){
		this.theoryRoot = root;
		po_node = new ScheduleModelElementNode(IPSStatus.ELEMENT_TYPE, this);
	}


	
	/**
	 * Processes the children of this Context:
	 * Clears existing axioms and theorems.
	 * Adds all axioms and theorems found in the internalContext root.
	 */
	public void processChildren(){
		// TODO children which have proof obligations needs to be processed
	}
	
	/**
	 * Processes the PORoot that belongs to this context.
	 * It creates a ModelProofObligation for each sequent
	 * and adds it to this context as well as to the
	 * concerned Theorems and Axioms.
	 */
	public void processPORoot() {
		if (poNeedsProcessing) {
			try {
				//clear old POs
				proofObligations.clear();
				IPORoot root = this.theoryRoot.getPORoot();
				if (root.exists()) {
					IPOSequent[] sequents = root.getSequents();
					int pos = 1;
					for (IPOSequent sequent : sequents) {
						ScheduleModelProofObligation po = new ScheduleModelProofObligation(sequent, pos);
						pos++;
						po.setSchedule(this);
						proofObligations.put(sequent, po);
			
						IPOSource[] sources = sequent.getSources();
						for (int j = 0; j < sources.length; j++) {
							IRodinElement source = sources[j].getSource();
							//only process sources that belong to this context.
							if (theoryRoot.isAncestorOf(source)) {
								processSource(source, po);
							}
						}
					}
				}
			} catch (RodinDBException e) {
				UIUtils.log(e, "when processing proof obligations of " +theoryRoot);
			}
			poNeedsProcessing = false;
		}
	}
	
	
	/**
	 * Processes the PSRoot that belongs to this Context. Each status is added to
	 * the corresponding Proof Obligation, if that ProofObligation is present.
	 */
	public void processPSRoot(){
		if (psNeedsProcessing) {
			try {
				IPSRoot root = this.theoryRoot.getPSRoot();
				if (root.exists()) {
					IPSStatus[] stats = root.getStatuses();
					for (IPSStatus status : stats) {
						IPOSequent sequent = status.getPOSequent();
						// check if there is a ProofObligation for this status (there should be one!)
						if (proofObligations.containsKey(sequent)) {
							proofObligations.get(sequent).setIPSStatus(status);
						}
					}
				}
			} catch (RodinDBException e) {
				// nothing serious
				//UIUtils.log(e, "when processing proof statuses of " +theoryRoot);
			}
			psNeedsProcessing = false;
		}
	}


	/**
	 * @return The Project that contains this Context.
	 */
	@Override
	public IModelElement getModelParent() {
		return ScheduleModelController.getProject(theoryRoot.getRodinProject());
	}
	
	
	/**
	 * process the proof obligations if needed
	 * 
	 * @return the total number of Proof Obligations
	 */
	@Override
	public int getPOcount(){
		if (poNeedsProcessing || psNeedsProcessing) {
			processPORoot();
			processPSRoot();
		}
		return proofObligations.size();
	}
	
	/**
	 * process the proof obligations if needed
	 * 
	 * @return The number of undischarged Proof Obligations
	 */
	@Override
	public int getUndischargedPOcount() {
		if (poNeedsProcessing || psNeedsProcessing) {
			processPORoot();
			processPSRoot();
		}
		int result = 0;
		for (ScheduleModelProofObligation po : proofObligations.values()) {
			if (!po.isDischarged()) {
				result++;
			}
		}
		return result;
		
	}
	

	@Override
	public IRodinElement getInternalElement() {
		return theoryRoot;
	}
	
	/**
	 * Processes a source belonging to a given Proof Obligation
	 * 
	 * @param source
	 *            The source to process
	 * @param po
	 *            The proof obligation the source belongs to
	 */
	protected void processSource (IRodinElement source, ScheduleModelProofObligation po) {
		
	}
	
	public IModelElement getModelElement(IRodinElement element) {
		return null;
	}

	/**
	 * In the complex version this gets the first abstract context of this
	 * context. If none exist or in the non-complex version this returns the
	 * containing project.
	 */
	@Override
	public Object getParent(boolean complex) {
		return theoryRoot.getRodinProject();
	}


	@Override
	public Object[] getChildren(IInternalElementType<?> type, boolean complex) {

		
		if (poNeedsProcessing || psNeedsProcessing) {
			processPORoot();
			processPSRoot();
		}
		if (type == IPSStatus.ELEMENT_TYPE) {
			return new Object[]{po_node};
		}
		
		if (ExplorerUtils.DEBUG) {
			System.out.println("Unsupported children type for theory: " +type);
		}
		return new Object[0];
	}

	public IEventBRoot getScheduleRoot() {
		return theoryRoot;
	}
	

}
