package ac.soton.codegen.ui.explorer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.ListenerList;
import org.eventb.core.IPORoot;
import org.eventb.core.IPSRoot;
import org.eventb.core.IPSStatus;
import org.rodinp.core.ElementChangedEvent;
import org.rodinp.core.IElementChangedListener;
import org.rodinp.core.IRodinDB;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IScheduleAuxRoot;
import fr.systerel.internal.explorer.model.IModelElement;
import fr.systerel.internal.explorer.model.IModelListener;
import fr.systerel.internal.explorer.navigator.ExplorerUtils;

public class ScheduleAuxModelController implements IElementChangedListener{
	private static ScheduleAuxModelController instance;
	private static HashMap<IRodinProject, ScheduleAuxModelProject> projects = new HashMap<IRodinProject, ScheduleAuxModelProject>();

	private final ListenerList listeners = new ListenerList();

	/**
	 * Create this controller and register it in the DataBase for changes.
	 * 
	 * @param viewer
	 */
	public ScheduleAuxModelController() {
		RodinCore.addElementChangedListener(this);
	}

	public void addListener(IModelListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the corresponding elements from the model
	 * 
	 * @param toRemove
	 */
	public void cleanUpModel(ArrayList<IRodinElement> toRemove) {
		for (IRodinElement element : toRemove) {
			if (element instanceof IScheduleAuxRoot) {
				removeSchedule((IScheduleAuxRoot) element);
			}
			if (element instanceof IRodinProject) {
				removeProject((IRodinProject) element);
			}
		}
	}

	/**
	 * React to changes in the database.
	 * 
	 */
	public void elementChanged(ElementChangedEvent event) {

		/*ScheduleDeltaProcessor processor = new ScheduleDeltaProcessor(
				event.getDelta());
		final ArrayList<IRodinElement> toRefresh = processor.getToRefresh();
		final ArrayList<IRodinElement> toRemove = processor.getToRemove();

		cleanUpModel(toRemove);
		// refresh the model
		for (IRodinElement elem : toRefresh) {
			refreshModel(elem);

		}
		notifyListeners(toRefresh);*/

	}

	/**
	 * Refreshes the model
	 * 
	 * @param element
	 *            The element to refresh
	 */
	public void refreshModel(IRodinElement element) {
		if (!(element instanceof IRodinDB)) {
			ScheduleAuxModelProject project = projects
					.get(element.getRodinProject());
			if (project != null) {
				if (element instanceof IRodinProject) {
					project.needsProcessing = true;
					processProject((IRodinProject) element);
				}
				if (element instanceof IScheduleAuxRoot) {
					project.processSchedule((IScheduleAuxRoot) element);
				}
				if (element instanceof IPORoot) {
					IPORoot root = (IPORoot) element;
					IScheduleAuxRoot thyRoot = getScheduleRoot(root.getElementName(),
							root.getRodinProject());
					if (thyRoot == null) {
						return;
					}
					if (thyRoot.exists()) {
						ModelSchedule theory = getSchedule(thyRoot);
						theory.poNeedsProcessing = true;
						theory.processPORoot();
						// process the statuses as well
						theory.psNeedsProcessing = true;
						theory.processPSRoot();
					}
				}
				if (element instanceof IPSRoot) {
					IPSRoot root = (IPSRoot) element;
					IScheduleAuxRoot thyRoot = getScheduleRoot(root.getElementName(),
							root.getRodinProject());
					if (thyRoot == null) {
						return;
					}
					if (thyRoot.exists()) {
						ModelSchedule theory = getSchedule(thyRoot);
						theory.psNeedsProcessing = true;
						theory.processPSRoot();
					}
				}
			}
		}
	}

	public void removeListener(IModelListener listener) {
		listeners.remove(listener);
	}

	private IScheduleAuxRoot getScheduleRoot(String name, IRodinProject proj) {
		IScheduleAuxRoot[] roots = null;
		try {
			roots = proj.getRootElementsOfType(IScheduleAuxRoot.ELEMENT_TYPE);
		} catch (RodinDBException e) {
			e.printStackTrace();
			return null;
		}
		for (IScheduleAuxRoot root : roots) {
			if (root.getElementName().equals(name)) {
				return root;
			}
		}
		return null;
	}

	private void notifyListeners(List<IRodinElement> toRefresh) {
		for (Object listener : listeners.getListeners()) {
			safeNotify((IModelListener) listener, toRefresh);
		}
	}

	private void safeNotify(IModelListener listener,
			List<IRodinElement> toRefresh) {
		try {
			listener.refresh(toRefresh);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void createInstance() {
		if (instance == null) {
			instance = new ScheduleAuxModelController();
		}
	}

	public static IModelElement getModelElement(Object element) {
		if (element instanceof IModelElement) {
			return (IModelElement) element;
		}
		if (element instanceof IRodinProject) {
			return projects.get(element);
		}
		if (element instanceof IRodinElement) {
			ScheduleAuxModelProject project = projects.get(((IRodinElement) element)
					.getRodinProject());
			if (project != null) {
				return project.getModelElement((IRodinElement) element);
			}
		}
		if (ExplorerUtils.DEBUG) {
			System.out.println("Element not found by ModelController: "
					+ element);
		}
		return null;
	}

	/**
	 * Gets the ModelProofObligation for a given IPSStatus
	 * 
	 * @param status
	 *            The IPSStatus to look for
	 * @return The corresponding ModelProofObligation, if there exists one,
	 *         <code>null</code> otherwise
	 */
	public static ScheduleModelProofObligation getModelPO(IPSStatus status) {
		ScheduleAuxModelProject project = projects.get(status.getRodinProject());
		if (project != null) {
			return project.getProofObligation(status);
		}
		return null;
	}

	/**
	 * Gets the ModelProject for a given RodinProject
	 * 
	 * @param project
	 *            The RodinProjecct to look for
	 * @return The corresponding ModelProject, if there exists one,
	 *         <code>null</code> otherwise
	 */
	public static ScheduleAuxModelProject getProject(IRodinProject project) {
		return projects.get(project);
	}

	public static ModelSchedule getSchedule(IScheduleAuxRoot theoryRoot) {
		ScheduleAuxModelProject project = projects.get(theoryRoot.getRodinProject());
		if (project != null) {
			return project.getSchedule(theoryRoot);
		}
		return null;
	}

	/**
	 * Processes a RodinProject. Creates a model for this project (Machines,
	 * Contexts, Invariants etc.). Proof Obligations are not included in
	 * processing.
	 * 
	 * @param project
	 *            The Project to process.
	 */
	public static void processProject(IRodinProject project) {
		try {
			ScheduleAuxModelProject prj;
			if (!projects.containsKey(project)) {
				prj = new ScheduleAuxModelProject(project);
				projects.put(project, prj);
			}
			prj = projects.get(project);
			// only process if really needed
			if (prj.needsProcessing) {
				IScheduleAuxRoot[] theories = getRootScheduleChildren(project);
				for (IScheduleAuxRoot theory : theories) {
					prj.processSchedule(theory);
				}
				prj.needsProcessing = false;
			}
		} catch (RodinDBException e) {
			//ScheduleUIUtils.log(e, "when processing project " + project);
		}
	}

	/**
	 * Removes the corresponding ModelProject from the Model if it was present.
	 * 
	 * @param project
	 *            The Project to remove.
	 */
	public static void removeProject(IRodinProject project) {
		projects.remove(project);
	}

	/**
	 * Removes a ModelSchedule from the Model for a given ScheduleRoot
	 * 
	 * @param contextRoot
	 *            The ContextRoot to remove
	 */
	public static void removeSchedule(IScheduleAuxRoot root) {
		ScheduleAuxModelProject project = projects.get(root.getRodinProject());
		if (project != null) {
			project.removeSchedule(root);
		}
	}

	private static IScheduleAuxRoot[] getRootScheduleChildren(IRodinProject project)
			throws RodinDBException {
		return project.getRootElementsOfType(IScheduleAuxRoot.ELEMENT_TYPE);
	}

}
