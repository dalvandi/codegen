package ac.soton.codegen.ui.explorer.model;

import org.eventb.core.IPSStatus;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;

import ac.soton.codegen.core.IScheduleAuxRoot;

public class ModelScheduleAux extends ScheduleAuxModelPOContainer{

	private IScheduleAuxRoot theoryRoot;

	
	public ModelScheduleAux(IScheduleAuxRoot root){
		this.theoryRoot = root;
	}

	@Override
	public IRodinElement getInternalElement() {
		// TODO Auto-generated method stub
		return theoryRoot;
	}

	@Override
	public Object getParent(boolean complex) {
		// TODO Auto-generated method stub
		return theoryRoot.getRodinProject();
	}

	@Override
	public Object[] getChildren(IInternalElementType<?> type, boolean complex) {
		// TODO Auto-generated method stub
		return new Object[0];
	}

}
