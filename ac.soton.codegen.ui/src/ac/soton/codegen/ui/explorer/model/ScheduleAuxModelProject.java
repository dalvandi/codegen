package ac.soton.codegen.ui.explorer.model;

import java.util.HashMap;

import org.eventb.core.IEventBRoot;
import org.eventb.core.IPSRoot;
import org.eventb.core.IPSStatus;
import org.eventb.internal.ui.UIUtils;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IScheduleAuxRoot;
import fr.systerel.internal.explorer.model.IModelElement;
import fr.systerel.internal.explorer.navigator.ExplorerUtils;

public class ScheduleAuxModelProject implements IModelElement{
	private IRodinProject internalProject;
	private HashMap<IScheduleAuxRoot, ModelSchedule> theories = new HashMap<IScheduleAuxRoot, ModelSchedule>();
	
	//indicates whether to projects needs to be processed freshly (process Machines etc.)
	public boolean needsProcessing =  true;

	public ScheduleAuxModelProject(IRodinProject project) {
		internalProject = project;
	}

	public Object[] getChildren(IInternalElementType<?> type, boolean complex) {
		
		try {
			if (type == IScheduleAuxRoot.ELEMENT_TYPE) {
				return internalProject.getRootElementsOfType(type);
			}
			} catch (RodinDBException e) {
				UIUtils.log(e, "when getting machines or contexts of " +internalProject);
			}
		
		if (ExplorerUtils.DEBUG) {
			System.out.println("Did not find children of type: "+type +"for project " +internalProject);
		}
		return new Object[0];
	}

	/**
	 * @return The number of manually discharged Proof Obligations of this project
	 */
	public int getManuallyDischargedPOcount() {
		int result = 0;
		for (ModelSchedule theory : theories.values()) {
			result += theory.getManuallyDischargedPOcount();
		}
		return result;
	}

	public IModelElement getModelElement(IRodinElement element) {

		if (element instanceof IScheduleAuxRoot) {
			return getSchedule((IScheduleAuxRoot) element);
		}
		IEventBRoot parent= element.getAncestor(IScheduleAuxRoot.ELEMENT_TYPE);
		ModelSchedule theory = theories.get(parent);
		if (theory != null) {
			return theory.getModelElement(element);
		}
		return null;

	}

	/**
	 * @return the total number of Proof Obligations of this project
	 */
	public int getPOcount(){
		int result = 0;
		for (ModelSchedule theory : theories.values()) {
			result += theory.getPOcount();
		}
		return result;
	}

	public ScheduleModelProofObligation getProofObligation(IPSStatus status){
		IEventBRoot root = (IEventBRoot) status.getRoot();
		if (root instanceof IPSRoot) {
			
			ModelSchedule theory = theories.get(root.getRoot());
			if (theory != null) {
				return theory.getProofObligation(status);
			}
		}
		return null;
	}
	
	/**
	 * @return The number of manually discharged Proof Obligations of this project
	 */
	public int getReviewedPOcount() {
		int result = 0;
		for (ModelSchedule theory : theories.values()) {
			result += theory.getReviewedPOcount();
		}
		return result;
		
	}
	
	public HashMap<IScheduleAuxRoot, ModelSchedule> getTheories() {
		return theories;
	}

	public ModelSchedule getSchedule(IScheduleAuxRoot theory) {
		return theories.get(theory);
	}
	
	/**
	 * @return The number of undischarged Proof Obligations of this project
	 */
	public int getUndischargedPOcount() {
		int result = 0;
		for (ModelSchedule theory : theories.values()) {
			result += theory.getUndischargedPOcount();
		}
		return result;
		
	}
	
	public void processSchedule(IScheduleAuxRoot theory) {
		ModelScheduleAux thy;
		if (!theories.containsKey(theory)) {
			thy = new ModelScheduleAux(theory);
			//theories.put(theory, thy);
		} else {
			//thy = theories.get(theory);
		}
		
	}
	
	public void removeSchedule(IScheduleAuxRoot theoryRoot) {
		ModelSchedule theory = theories.get(theoryRoot);
		if (theory != null) {
			theories.remove(theoryRoot);
		}
	}

	@Override
	public IModelElement getModelParent() {
		return null;
	}

	@Override
	public IRodinElement getInternalElement() {
		return internalProject;
	}

	@Override
	public Object getParent(boolean complex) {
		return null;
	}

}
