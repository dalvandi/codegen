package ac.soton.codegen.ui.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eventb.core.IEventBRoot;
import ac.soton.codegen.core.IScheduleRoot;
import ac.soton.codegen.ui.explorer.model.ScheduleModelController;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

public class ScheduleContentProvider implements ITreeContentProvider {

	private static final Object[] NO_OBJECT = new Object[0];
	
	protected final IInternalElementType<? extends IEventBRoot> rootType;
	
	public ScheduleContentProvider() {
		rootType = IScheduleRoot.ELEMENT_TYPE;
	}
	
	public void dispose() {
		// ignore
	}

	public Object[] getChildren(Object element) {
		// ensure the existence on the instance
		ScheduleModelController.createInstance();
		if (element instanceof IProject) {
			IRodinProject proj = RodinCore.valueOf((IProject) element);
			if (proj.exists()) {
				ScheduleModelController.processProject(proj);
				try {
					return getRootChildren(proj);
				} catch (RodinDBException e) {
					//ScheduleUIUtils.log(e, "when accessing " + rootType.getName()
					//		+ " roots of " + proj);
				}
			}
		}
		return NO_OBJECT;
	}

	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	public boolean hasChildren(Object element) {
		if (element instanceof IProject) {
			IRodinProject proj = RodinCore.valueOf((IProject) element);
			if (proj.exists()) {
				try {
					return getRootChildren(proj).length > 0;
				} catch (RodinDBException e) {
					return false;
				}
			}
		}
		return false;
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		
	}

	public Object getParent(Object element) {
		if (element instanceof IScheduleRoot) {
			return ((IScheduleRoot) element).getParent().getParent();
		}
		return null;
	}

	protected IEventBRoot[] getRootChildren(IRodinProject project)
			throws RodinDBException {
		return (IScheduleRoot[]) getScheduleChildren(project);
	}

	private IRodinElement[] getScheduleChildren(IRodinProject proj) {
		try {
			return proj.getRootElementsOfType(IScheduleRoot.ELEMENT_TYPE);
		} catch (RodinDBException e) {
			//ScheduleUIUtils.log(e, "Error while retrieving "
			//		+ IScheduleRoot.ELEMENT_TYPE + " from " + proj);
			e.printStackTrace();
			return null;
		}
	}

}
