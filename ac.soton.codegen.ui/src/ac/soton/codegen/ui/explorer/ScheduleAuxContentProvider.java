package ac.soton.codegen.ui.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eventb.core.IEventBRoot;
import org.rodinp.core.IInternalElementType;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.IRodinProject;
import org.rodinp.core.RodinCore;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IScheduleAuxRoot;
import ac.soton.codegen.core.IScheduleRoot;
import ac.soton.codegen.ui.explorer.model.ScheduleAuxModelController;
import ac.soton.codegen.ui.explorer.model.ScheduleModelController;

public class ScheduleAuxContentProvider implements ITreeContentProvider {
	
	private static final Object[] NO_OBJECT = new Object[0];
	
	protected final IInternalElementType<? extends IEventBRoot> rootType;
	
	public ScheduleAuxContentProvider() {
		rootType = IScheduleAuxRoot.ELEMENT_TYPE;
	}
	@Override
	public Object[] getElements(Object inputElement) {
		// TODO Auto-generated method stub
		return NO_OBJECT;
	}

	@Override
	public Object[] getChildren(Object element) {
		ScheduleAuxModelController.createInstance();
		if (element instanceof IProject) {
			IRodinProject proj = RodinCore.valueOf((IProject) element);
			if (proj.exists()) {
				ScheduleAuxModelController.processProject(proj);
				try {
					return getRootChildren(proj);
				} catch (RodinDBException e) {
					//ScheduleUIUtils.log(e, "when accessing " + rootType.getName()
					//		+ " roots of " + proj);
				}
			}
		}
		return NO_OBJECT;
	}

	protected IEventBRoot[] getRootChildren(IRodinProject project)
			throws RodinDBException {
		return (IScheduleAuxRoot[]) getScheduleAuxChildren(project);
	}

	private IRodinElement[] getScheduleAuxChildren(IRodinProject proj) {
		try {
			return proj.getRootElementsOfType(IScheduleAuxRoot.ELEMENT_TYPE);
		} catch (RodinDBException e) {
			//ScheduleUIUtils.log(e, "Error while retrieving "
			//		+ IScheduleRoot.ELEMENT_TYPE + " from " + proj);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof IScheduleAuxRoot) {
			return ((IScheduleAuxRoot) element).getParent().getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return false;
	}

}
