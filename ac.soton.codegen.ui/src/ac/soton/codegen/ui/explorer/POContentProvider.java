package ac.soton.codegen.ui.explorer;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eventb.core.IPSStatus;
import org.rodinp.core.IInternalElementType;

import fr.systerel.internal.explorer.model.IModelElement;

@SuppressWarnings("restriction")
public class POContentProvider {
	
	public static class ProofObligationContentProvider extends AbstractContentProvider {

		public ProofObligationContentProvider() {
			super(IPSStatus.ELEMENT_TYPE);
		}
	}
	

}

@SuppressWarnings("restriction")
abstract class AbstractContentProvider implements ITreeContentProvider {

	protected static final Object[] NO_OBJECT = new Object[0];
	
	protected final IInternalElementType<?> type;

	public AbstractContentProvider(IInternalElementType<?> type) {
		this.type = type;
	}
	
	@Override
	public Object[] getChildren(Object element) {
		return NO_OBJECT;
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}
	
	/**
	 * Override this to change the parent behaviour.
	 */
	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public void dispose() {
		// ignore
	
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// ignore
	}

}