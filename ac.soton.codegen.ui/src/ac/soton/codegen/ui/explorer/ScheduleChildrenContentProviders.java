package ac.soton.codegen.ui.explorer;

import java.text.Collator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eventb.core.IPSStatus;
import org.rodinp.core.IInternalElementType;

import ac.soton.codegen.ui.explorer.model.ScheduleModelController;

import org.eclipse.jface.viewers.ITreeContentProvider;
import fr.systerel.internal.explorer.model.IModelElement;

@SuppressWarnings({ "deprecation", "restriction" })
public class ScheduleChildrenContentProviders {
	public static class AntiSorter extends ViewerSorter {

		public AntiSorter() {
		}

		public AntiSorter(Collator collator) {
			super(collator);
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			return 0;
		}

	}
	public static class POContentProvider extends AbstractContentProvider {

		public POContentProvider() {
			super(IPSStatus.ELEMENT_TYPE);
		}
	}

	
	@SuppressWarnings("restriction")
	static abstract class AbstractContentProvider implements ITreeContentProvider {

		protected static final Object[] NO_OBJECT = new Object[0];
		
		protected final IInternalElementType<?> type;

		public AbstractContentProvider(IInternalElementType<?> type) {
			this.type = type;
		}
		
		@Override
		public Object[] getChildren(Object element) {
			IModelElement model = ScheduleModelController.getModelElement(element);
			if (model != null) {
				return model.getChildren(type, false);
			}
			return NO_OBJECT;
		}

		@Override
		public boolean hasChildren(Object element) {
			return getChildren(element).length > 0;
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return getChildren(inputElement);
		}
		
		/**
		 * Override this to change the parent behaviour.
		 */
		@Override
		public Object getParent(Object element) {

			IModelElement model = ScheduleModelController.getModelElement(element);
			if (model != null) {
				return model.getParent(true);
			}
			return null;
		}

		@Override
		public void dispose() {
			// ignore
		
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			// ignore
		}

	}

}
