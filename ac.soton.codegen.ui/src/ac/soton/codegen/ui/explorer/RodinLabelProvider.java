package ac.soton.codegen.ui.explorer;

import org.eclipse.core.resources.IContainer;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eventb.core.IIdentifierElement;
import org.eventb.core.ILabeledElement;
import org.eventb.core.IPSStatus;
import org.eventb.core.seqprover.IConfidence;
import org.rodinp.core.IRodinElement;
import org.rodinp.core.RodinDBException;

import ac.soton.codegen.core.IScheduleAuxRoot;
import ac.soton.codegen.core.IScheduleRoot;
import ac.soton.codegen.ui.explorer.model.ScheduleModelElementNode;
import ac.soton.codegen.ui.explorer.model.ScheduleModelPOContainer;
import ac.soton.codegen.ui.internal.ui.IScheduleImages;
import ac.soton.codegen.ui.internal.ui.ScheduleImage;
import fr.systerel.explorer.IElementNode;

public class RodinLabelProvider extends DecoratingLabelProvider{

	public RodinLabelProvider() {
		super(new LblProv(), PlatformUI.getWorkbench().getDecoratorManager()
				.getLabelDecorator());
	}
	
	private static class LblProv implements ILabelProvider {

		public LblProv() {
			// avoid synthetic accessor
		}

		@Override
		public Image getImage(Object element) {
			if (element instanceof IPSStatus) {
				IPSStatus status = ((IPSStatus) element);
				if(status.exists())
					return ScheduleImage.getPRSequentImage(status);
			}
			if(element instanceof IScheduleRoot)
			{
				return ScheduleImage.getImage("Schedule");
			}
			if(element instanceof IScheduleAuxRoot)
			{
				return ScheduleImage.getImage("ScheduleAux");
			}
			if (element instanceof IRodinElement) {
				return ScheduleImage.getRodinImage((IRodinElement) element);
			}
			else if (element instanceof IElementNode) {
				IElementNode node = (IElementNode) element;
				if (node.getChildrenType()==IPSStatus.ELEMENT_TYPE) {
					if(node.getParent().getElementType()!=IScheduleRoot.ELEMENT_TYPE)
						return null;
					ScheduleModelPOContainer parent = ((ScheduleModelElementNode) node)
							.getModelParent();
					boolean discharged = parent.getMinConfidence() > IConfidence.REVIEWED_MAX;
					boolean reviewed = parent.getMinConfidence() > IConfidence.PENDING;
					boolean unattempted = parent.getMinConfidence() == IConfidence.UNATTEMPTED;

					if (discharged) {
						return ScheduleImage
								.getImage(IScheduleImages.IMG_DISCHARGED);
					} else if (reviewed) {
						return ScheduleImage
								.getImage(IScheduleImages.IMG_REVIEWED);
					} else if (unattempted) {
						return ScheduleImage
								.getImage(IScheduleImages.IMG_PENDING_PALE);
					} else {
						return ScheduleImage
								.getImage(IScheduleImages.IMG_PENDING);
					}
				}
				
			}else if (element instanceof IContainer) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_INFO_TSK);
			}
			return null;
		}
		 
		@Override
		public String getText(Object obj) {
			if (obj instanceof ILabeledElement) {
				try {
					return ((ILabeledElement) obj).getLabel();
				} catch (RodinDBException e) {
					//UIUtils.log(e, "when getting label for " + obj);
				}
			} else if (obj instanceof IIdentifierElement) {
				try {
					return ((IIdentifierElement) obj).getIdentifierString();
				} catch (RodinDBException e) {
					//UIUtils.log(e, "when getting identifier for " + obj);
				}
			} else if (obj instanceof IRodinElement) {
				return ((IRodinElement) obj).getElementName();

			} else if (obj instanceof IElementNode) {
				return ((IElementNode) obj).getLabel();

			} else if (obj instanceof IContainer) {
				return ((IContainer) obj).getName();
			}
			return obj.toString();
		}

		

		@Override
		public void dispose() {
			// do nothing

		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}


		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

	}

}
