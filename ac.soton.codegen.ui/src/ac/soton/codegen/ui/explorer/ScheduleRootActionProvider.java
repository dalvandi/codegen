package ac.soton.codegen.ui.explorer;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionConstants;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonMenuConstants;
import org.rodinp.core.IInternalElement;
import ac.soton.codegen.ui.plugin.CodegenUIPlugIn;
import fr.systerel.internal.explorer.navigator.actionProviders.ActionCollection;

public class ScheduleRootActionProvider extends CommonActionProvider{
	protected ICommonActionExtensionSite site;
	protected StructuredViewer viewer;
	private static String GROUP_META = "meta";
	private String GROUP_DELETE = "delete";;

	public void init(ICommonActionExtensionSite aSite) {
		super.init(aSite);
		site = aSite;
		viewer = aSite.getStructuredViewer();
	}
	
	
	public MenuManager buildOpenWithMenu() {
		MenuManager menu = new MenuManager("Open With", ICommonMenuConstants.GROUP_OPEN_WITH);
		ISelection selection = site.getStructuredViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		//menu.add(new OpenWithMenu(CodegenUIPlugIn.getActivePage(), ((IInternalElement) obj).getRodinFile().getResource()));
		return menu;
	}
	
	@SuppressWarnings("restriction")
	public void fillActionBars(IActionBars actionBars) {
		// forward doubleClick to doubleClickAction
		actionBars.setGlobalActionHandler(ICommonActionConstants.OPEN, ActionCollection.getOpenAction(site));
		// forwards pressing the delete key to deleteAction
		//actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(), getDeleteAction(site));
	}

	@SuppressWarnings("restriction")
	public void fillContextMenu(IMenuManager menu) {
		menu.appendToGroup(ICommonMenuConstants.GROUP_OPEN, ActionCollection.getOpenAction(site));
		menu.appendToGroup(ICommonMenuConstants.GROUP_OPEN_WITH, buildOpenWithMenu());
		//menu.add(new Separator(GROUP_META));
		//menu.appendToGroup(GROUP_META, getDeployTheoryAction());
		//menu.appendToGroup(GROUP_META, getUndeployTheoryAction());
		//menu.add(new Separator(GROUP_DELETE));
		//menu.appendToGroup(GROUP_DELETE, getDeleteAction(site));
	}

}
