package ac.soton.codegen.ui.plugin;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import ac.soton.codegen.ui.internal.ui.ScheduleImage;

public class CodegenUIPlugIn extends AbstractUIPlugin{
	public static final String PLUGIN_ID = "ac.soton.codegen.ui";
	// The shared instance
	private static CodegenUIPlugIn plugin;
	
	public CodegenUIPlugIn() {
		super();
		plugin = this;
	}
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}
	
	public static IWorkbenchPage getActivePage() {
		return getDefault().internalGetActivePage();
	}

	public static CodegenUIPlugIn getDefault() {
		return plugin;
	}
	
	private IWorkbenchPage internalGetActivePage() {
		return getWorkbench().getActiveWorkbenchWindow().getActivePage();
	}

	protected void initializeImageRegistry(ImageRegistry reg) {

		ScheduleImage.initializeImageRegistry(reg);
		super.initializeImageRegistry(reg);
	}
}
